/*.-----------------------------------------------------------------.
  .    ____                          __                             .
  .   /\  _`\                       /\ \__  __                      .
  .   \ \ \/\_\  _ __    __     __  \ \ ,_\/\_\  __  __     __      .
  .    \ \ \/_/_/\`'__\/'__`\ /'__`\ \ \ \/\/\ \/\ \/\ \  /'__`\    .
  .     \ \ \s\ \ \ \//\  __//\ \d\.\_\ \ \_\ \ \ \ \_/ |/\  __/    .
  .      \ \____/\ \_\\ \____\ \__/.\_\\ \__\\ \_\ \___/ \ \____\   .
  .       \/___/  \/_/ \/____/\/__/\/_/ \/__/ \/_/\/__/   \/____/   .
  .                                                                 .
  .          2014~2018 � Creative Services and Development          .
  .                      www.creativesd.com.br                      .
  .-----------------------------------------------------------------.
  . Script:                                                         .
  .    Costume System                                               .
  .-----------------------------------------------------------------.
  . Autor: Romulo SM (sbk_)                            Vers�o: 1.0  .
  .                                                                 .
  . Comp�tivel:	rAthena e deriva��es.                               .
  .-----------------------------------------------------------------.
  . Descri��o:                                                      .
  .    Converte itens para visual e possui uma loja.                .
  *-----------------------------------------------------------------*/
prontera,141,182,5	script	Itens Visuais#main	4_F_FRUIT,{
	set .@loop_1,1;
	while(.@loop_1)
	{
		mes "^0000FF[Itens Visuais]^000000";
		mes "Ol�! Posso converter seus ^0000FFHeadgears^000000 em Itens Visuais, tamb�m tenho alguns a venda.";
		mes "Em que posso ajudar?";
		next;
		switch( select("- Converter Item.", (.CostumeShop?"^0000FF- Loja de Itens Visuais.^000000":""), "^FF0000- Cancelar.^000000") )
		{
			case 1:
				set .@loop_2,1;
				while(.@loop_2)
				{
					set .@count, 0;
					set .@buildmenu$, "";
					setarray .@equippos[0], EQI_HEAD_TOP, EQI_HEAD_MID, EQI_HEAD_LOW;
					setarray .@equipname$[0], "Topo", "Meio", "Baixo";
					for( set .@i,0; .@i < getarraysize(.@equippos); set .@i, .@i + 1 )
					{
						if( getequipisequiped(.@equippos[.@i]) ) {
							set .@buildmenu$, .@buildmenu$ + "- " + getequipname(.@equippos[.@i]) + " ^0000FF[" + .@equipname$[.@i] + "]^000000.";
							set .@count,.@count+1;
						}
						set .@buildmenu$, .@buildmenu$ + ":";
					}
					
					set .@buildmenu$, .@buildmenu$ + "^FFA500- Voltar.^000000:^FF0000- Cancelar.^000000";
					
					if( .@count <= 0 )
					{
						mes "^0000FF[Itens Visuais]^000000";
						mes "Voc� n�o possu� nenhum ^0000FFHeadgear^000000 equipado.";
						next;
						break;
					}

					mes "^0000FF[Itens Visuais]^000000";
					mes "Muito bem, agora selecione o ^0000FFequipamento^000000 que deseja converter.";
					next;
					set .@i, select(.@buildmenu$)-1;
					if( .@i >= getarraysize(.@equippos) ) {
						set .@loop_2,0;
						if( .@i > getarraysize(.@equippos) )
							set .@loop_1,0;
						break;
					}
					
					mes "^0000FF[Itens Visuais]^000000";
					mes "Voc� escolheu ^0000FF" + getequipname(.@equippos[.@i]) + "^000000 para a convers�o.";
					if( .ZenyCost ) {
						mes "� necess�rio ^FF0000" + .ZenyCost + " Zeny^000000 para convers�o.";
						if( Zeny < .ZenyCost ) {
							mes "^FF0000Voc� n�o tem Zeny suficiente.^000000";
							next;
							break;
						}
					}
					mes " ";
					mes "Deseja inciar a convers�o?";
					next;
					switch( select("- Sim, por favor.", "^FFA500- N�o, obrigado.^000000", "^FF0000- Cancelar.^000000") )
					{
						case 1:
							if( .ZenyCost ) set Zeny, Zeny - .ZenyCost;
							if( .CardRefund ) {
								for( set .@c, 0; .@c < getequipcardcnt(.@equippos[.@i]); set .@c,.@c+1 ) {
									set .@cardid, getequipcardid(.@equippos[.@i],.@c);
									if( .@cardid )
										getitem .@cardid, 1;
								}
							}
							set .@itemname$, getequipname(.@equippos[.@i]);
							emotion ET_SMILE;
							costume .@equippos[.@i];
							specialeffect EF_CONCENTRATION;
							mes "^0000FF[Itens Visuais]^000000";
							mes "Muito bem, seu ^0000FF" + .@itemname$ + "^000000 foi convertido para visual!";
							next;
							break;
						case 2:
							break;
						case 3:
							set .@loop_1, 0;
							set .@loop_2, 0;
							break;
					}
				}
				break;
			case 2:
				mes "^0000FF[Itens Visuais]^000000";
				mes "Muito bem, vou abrir a ^0000FFLoja de Itens Visuais^000000.";
				close2;
				npcshopattach "Costume Shop#shop";
				callshop "Costume Shop#shop", 1;
				end;
			case 3:
				set .@loop_1,0;
				break;
		}
	}
	emotion ET_SMILE;
	mes "^0000FF[Itens Visuais]^000000";
	mes "Tudo bem, volte quando desejar mudar um pouco se visual!";
	close;
	
OnBuyItem:
	set .@CID, getbattleflag("costumeitem_reserved_id");
	if( .@CID <= 0 ) {
		emotion ET_SORRY;
		mes "^0000FF[Itens Visuais]^000000";
		mes "Ahhh! A loja n�o abriu, tente novamente mais tarde.";
		close;
	}
	
	if( Zeny < @bought_cost ) {
		emotion ET_SORRY;
		mes "^0000FF[Itens Visuais]^000000";
		mes "Voc� n�o tem Zeny suficiente para comprar este(s) iten(s).";
		close;
	}
	
	mes "^0000FF[Itens Visuais]^000000";
	freeloop(1);
	for( set .@i, 0; .@i < getarraysize(@bought_nameid); set .@i, .@i + 1 ) {
		if( !checkweight(@bought_nameid[.@i],@bought_quantity[.@i]) ) {
			emotion ET_KEK;
			mes "Voc� est� carregando muitos itens, as ^0000FFKafras^000000 podem te ajudar...";
			mes "Guarde alguns dos seus itens no Armaz�m Pessoal.";
			close;
		}
	}
	
	for( set .@i, 0; .@i < getarraysize(@bought_nameid); set .@i, .@i + 1 )
		getcostumeitem @bought_nameid[.@i], @bought_quantity[.@i];
		
	freeloop(0);
		
	set Zeny, Zeny - @bought_cost;
	emotion ET_THANKS;
	mes "Muito bem!! Aqui est�o seus itens.";
	mes "Volte quando desejar comprar algum item em nossa loja.";
	close;
	
OnInit:
	// Configura��es B�sicas
	//
	// Custo em Zeny para converter um equipamento em um item visual.
	// 0 Desativa esta op��o.
	set .ZenyCost, 1000;
	
	// Devolver cartas ao converter o item para visual?
	// 0 Desativa esta op��o.
	set .CardRefund, 0;
	
	// Habilitar Loja de Itens Visuais?
	set .CostumeShop, 1;
	end;
}

-	shop	Costume Shop#shop	-1,5086:100000,5596:100000,5794:100000,5258:200000,5474:200000,5207:200000,5137:300000,5766:300000,5786:300000,5389:400000,2254:400000,5074:400000,5132:400000