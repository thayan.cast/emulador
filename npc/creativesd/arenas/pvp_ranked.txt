/*.-----------------------------------------------------------------.
  .    ____                          __                             .
  .   /\  _`\                       /\ \__  __                      .
  .   \ \ \/\_\  _ __    __     __  \ \ ,_\/\_\  __  __     __      .
  .    \ \ \/_/_/\`'__\/'__`\ /'__`\ \ \ \/\/\ \/\ \/\ \  /'__`\    .
  .     \ \ \s\ \ \ \//\  __//\ \d\.\_\ \ \_\ \ \ \ \_/ |/\  __/    .
  .      \ \____/\ \_\\ \____\ \__/.\_\\ \__\\ \_\ \___/ \ \____\   .
  .       \/___/  \/_/ \/____/\/__/\/_/ \/__/ \/_/\/__/   \/____/   .
  .                                                                 .
  .          2014~2018 � Creative Services and Development          .
  .                      www.creativesd.com.br                      .
  .-----------------------------------------------------------------.
  . Script:                                                         .
  .    Arena PvP Ranqueada                                          .
  .-----------------------------------------------------------------.
  . Autor: Romulo SM (sbk_)                            Vers�o: 3.0  .
  .                                                                 .
  . Comp�tivel:	eAthena e deriva��es.                    	        .
  .-----------------------------------------------------------------.
  . Descri��o:                                                      .
  .    Arenas Balanceada de acordo com os Grupos de Habilidades.    .
  *-----------------------------------------------------------------*/
  
-	script	EventoRanckedSat#config	-1,{
OnInit:
	OnSat2256:
			enablenpc "PvP Ranqueada";
			disablenpc "PvP";
			//announce "[Rancked]: A Arena Ranqueada est� ligada.",bc_all,"0x00FF00";
			end;
		
	OnSat2257:
			disablenpc "PvP Ranqueada";
			enablenpc "PvP";
			//announce "[Rancked]: A Arena Ranqueada est� desligada.",bc_all,"0x00FF00";
				if (getmapusers("pvp_r_1-1") != 0) {
					mapwarp "pvp_r_1-1","prontera",150,150;
					end;
				} 
			end;
}

-	script	EventoRancked#config	-1,{
OnInit:
// N?vel de GM para poder ligar/desligar o Evento atraves do comando @mvpevent
	.AdmLevel = 99;
	
	disablenpc "PvP Ranqueada";
	enablenpc "PvP";
	if (getmapusers("pvp_r_1-1") != 0) {
	mapwarp "pvp_r_1-1","prontera",150,150;
	}
	bindatcmd "rancked", strnpcinfo(0)+"::OnCmdRancked", .AdmLevel, .AdmLevel;
	end;
	
OnCmdRancked:
	if( !.@atcmd_numparameters ) {
		message strcharinfo(0), "Voce deve digitar @rancked ligar/desligar neste comando.";
	}	else if( .@atcmd_parameters$[0] == "ligar" ) {
		enablenpc "PvP Ranqueada";
		disablenpc "PvP";
		announce "[Rancked]: A Arena Ranqueada est� ligada.",bc_all,"0x00FF00";
		end;
	}	else if( .@atcmd_parameters$[0] == "desligar" ) {
		disablenpc "PvP Ranqueada";
		enablenpc "PvP";
		announce "[Rancked]: A Arena Ranqueada est� desligada.",bc_all,"0x00FF00";
		if (getmapusers("pvp_r_1-1") != 0) {
		mapwarp "pvp_r_1-1","prontera",150,150;
		end;
		} 
		end;
	} else {
		message strcharinfo(0), "As opcoes validas sao ligar/desligar neste comando.";
		end;
	}

}

prontera,153,193,5	script	PvP	4_M_EIN_SOLDIER,{
	// Configura��es de Arenas N�o Ranqueadas
	//
	// Estrutura do Array
	//	"Arena_Name", <Min_BaseLevel>, <Max_BaseLevel>, <Max_User>, "<mapname>", <coord_x>, <coord_y>
	//
	//	Arena_Name: Nome da Arena.
	//	Min_BaseLevel: N�vel de Base m�nimo para entrar na Arena.
	//	Max_BaseLevel: N�vel de Base m�ximo para entrar na Arena.
	//	Max_User: M�ximo de Usu�rios que podem estar na Arena.
	//	mapname: Mapa aonde os jogadores ser�o teleportados.
	//	coord_x: Coordenada X aonde os jogadores ser�o teleportados. (0: ser� aleat�rio)
	//	coord_y: Coordenada Y aonde os jogadores ser�o teleportados. (0: ser� aleat�rio)
	
	//
	// * Arena PvP
	//
	setarray .@PvPUnrank_Arenas$[0],
		"Arena PvP 1", 1, 500, 30, "pvp_u_1-1", 0, 0;			// Arena PvP 1
		//"Arena PvP 2", 1, 500, 0, "pvp_u_2-1", 0, 0,			// Arena PvP 2
		//"Arena PvP 3", 1, 500, 0, "pvp_u_3-1", 0, 0,			// Arena PvP 3
		//"Arena PvP 4", 1, 500, 0, "pvp_u_4-1", 0, 0				// Arena PvP 4
		//;
		
	
	cutin "ein_soldier", 2;
	set .@loop_1,1;
	while(.@loop_1) {
		mes "^8B4513[Porteiro PvP]^000000";
		mes "Bem-vindo a Arena PvP!";
		mes "Em que posso ajudar?";
		next;
			// PvP N�o Ranqueado
			set .@w,0;
			set .@loop_2,1;
			while(.@loop_2) {
				set .@w, callsub(sub_Warper, 0, 0, "Arena PvP", .@PvPUnrank_Arenas$, getarraysize(.@PvPUnrank_Arenas$));
				break;
			}
	}
	mes "^8B4513[Porteiro PvP]^000000";
	mes "Muito bem, volte quando desejar entrar na ^0000FFArena PvP^000000.";
	close2;
	cutin "", 255;
	end;
	
sub_Warper:
	deletearray .@Arenas$;
	copyarray .@Arenas$, getarg(3), getarg(4);
	set .@loop_3,1;
	while(.@loop_3) {
		set .@n,0;
		set .@buildmenu$, "";
		mes "^8B4513[Porteiro PvP]^000000";
		mes "Escolha a ^0000FF" + getarg(2) + "^000000 que deseja acessar:";
		set .@c, 0;
		set .@b, getarg(0) ? 3 : 1;
		set .@n, getarg(0) ? 9 : 7;
		for( set .@i,0; .@i < getarraysize(.@Arenas$); set .@i, .@i + .@n ) {	
			set .@buildmenu$, .@buildmenu$+"^0000FF- " + .@Arenas$[.@i] + "^000000, Nv. ";
			
			if( atoi(.@Arenas$[.@i+.@b]) < 1 )
				set .@buildmenu$, .@buildmenu$ + 1;
			else
				set .@buildmenu$, .@buildmenu$ + atoi(.@Arenas$[.@i+.@b]);
				
			set .@buildmenu$, .@buildmenu$ + "~";
			
			set .@max_level, atoi(.@Arenas$[.@i+.@b+1]);
			if( .@max_level < 1 || .@max_level > MAX_LEVEL )
				set .@buildmenu$, .@buildmenu$ + MAX_LEVEL;
			else
				set .@buildmenu$, .@buildmenu$ + atoi(.@Arenas$[.@i+.@b+1]);
				
			set .@buildmenu$, .@buildmenu$+" ^FF0000[" + getmapusers(.@Arenas$[.@i+.@b+3]);
			
			if( atoi(.@Arenas$[.@i+.@b+2]) > 0 )
				set .@buildmenu$, .@buildmenu$ + "/" + atoi(.@Arenas$[.@i+.@b+2]);
						
			set .@buildmenu$, .@buildmenu$ + "]^000000.";
			set .@buildmenu$, .@buildmenu$ + ":";
			set .@c,.@c+1;
		}
		
		set .@buildmenu$, .@buildmenu$ + "^FFA500- Voltar.:^FF0000- Cancelar.^000000";
		set .@choose, select(.@buildmenu$)-1;

		if( .@choose == .@c )
			return 1;
					
		if( .@choose > .@c )
			return 0;

		set .@i, .@choose*.@n;
		set .@name$, .@Arenas$[.@i];
		set .@min_level, atoi(.@Arenas$[.@i+.@b]);
		set .@max_level, atoi(.@Arenas$[.@i+.@b+1]);
		set .@max_players, atoi(.@Arenas$[.@i+.@b+2]);
		set .@mapname$, .@Arenas$[.@i+.@b+3];
		set .@coord_x, atoi(.@Arenas$[.@i+.@b+4]);
		set .@coord_y, atoi(.@Arenas$[.@i+.@b+5]);
		
		if( .@min_level < 1 )
			set .@min_level, 1;
		if( .@max_level > MAX_LEVEL )
			set .@max_level, MAX_LEVEL;
		
		mes "^8B4513[Porteiro PvP]^000000";
			mes "Tem certeza que deseja entrar na ^0000FF" + .@name$ + (!getarg(0) ?" n�o ":" ") + "Ranqueada^000000?";
			next;
			switch( select("- Sim, por favor.", "^FF0000- Cancelar.^000000") ) {
				case 1:
					if( .@max_user && getmapusers(.@mapname$) > .@max_players ) {
						mes "^8B4513[Porteiro PvP]^000000";
						mes "A ^0000FF" + .@name$ + "^000000 est� cheia, por favor tente mais tarde.";
						next;
						break;
					}
					cutin "", 255;
					warp .@mapname$, .@coord_x, .@coord_y;
					end;
				case 2:
					mes "At� mais.";
					close2;
					cutin "", 255;
					end;
			}
	}
	return 0;
}

prontera,153,193,5	script	PvP Ranqueada	4_M_EIN_SOLDIER,{
	// Configura��es de Arenas N�o Ranqueadas
	//
	// Estrutura do Array
	//	"Arena_Name", <Min_BaseLevel>, <Max_BaseLevel>, <Max_User>, "<mapname>", <coord_x>, <coord_y>
	//
	//	Arena_Name: Nome da Arena.
	//	Min_BaseLevel: N�vel de Base m�nimo para entrar na Arena.
	//	Max_BaseLevel: N�vel de Base m�ximo para entrar na Arena.
	//	Max_User: M�ximo de Usu�rios que podem estar na Arena.
	//	mapname: Mapa aonde os jogadores ser�o teleportados.
	//	coord_x: Coordenada X aonde os jogadores ser�o teleportados. (0: ser� aleat�rio)
	//	coord_y: Coordenada Y aonde os jogadores ser�o teleportados. (0: ser� aleat�rio)
	
	//
	// * Arena PvP
	//
	setarray .@PvPUnrank_Arenas$[0],
		"Arena PvP 1", 1, 500, 30, "pvp_u_1-1", 0, 0;			// Arena PvP 1
		//"Arena PvP 2", 1, 500, 0, "pvp_u_2-1", 0, 0,			// Arena PvP 2
		//"Arena PvP 3", 1, 500, 0, "pvp_u_3-1", 0, 0,			// Arena PvP 3
		//"Arena PvP 4", 1, 500, 0, "pvp_u_4-1", 0, 0				// Arena PvP 4
		//;
	
	//
	// * Arena GvG
	//
	//setarray .@GvGUnrank_Arenas$[0],
	//	"Arena GvG 1", 1, 500, 0, "pvp_u_1-2", 0, 0,			// Arena GvG 1
	//	"Arena GvG 2", 1, 500, 0, "pvp_u_2-2", 0, 0,			// Arena GvG 2
	//	"Arena GvG 3", 1, 500, 0, "pvp_u_3-2", 0, 0,			// Arena GvG 3
	//	"Arena GvG 4", 1, 500, 0, "pvp_u_4-2", 0, 0				// Arena GvG 4
	//;
	
	//
	// * Arena GvG
	//
	//setarray .@PvP2Unrank_Arenas$[0],
	//	"Arena de Grupos 1", 1, 500, 0, "pvp_u_1-3", 0, 0,		// Arena de Grupos 1
	//	"Arena de Grupos 2", 1, 500, 0, "pvp_u_2-3", 0, 0,		// Arena de Grupos 2
	//	"Arena de Grupos 3", 1, 500, 0, "pvp_u_3-3", 0, 0,		// Arena de Grupos 3
	//	"Arena de Grupos 4", 1, 500, 0, "pvp_u_4-3", 0, 0		// Arena de Grupos 4
	//;
		
	
	// Configura��es de Arenas Ranqueadas
	//
	// Estrutura do Array
	//	"Arena_Name", RankID_Min, RankID_Max, <Min_BaseLevel>, <Max_BaseLevel>, <Max_User>, "<mapname>", <coord_x>, <coord_y>
	//
	//	Arena_Name: Nome da Arena.
	//	RankID_Min: Rank m�nimo para entrar na Arena. (0: N�o Ranqueados)
	//	RankID_Max: Rank m�ximo para entrar na Arena.
	//	Min_BaseLevel: N�vel de Base m�nimo para entrar na Arena.
	//	Max_BaseLevel: N�vel de Base m�ximo para entrar na Arena.
	//	Max_User: M�ximo de Usu�rios que podem estar na Arena.
	//	mapname: Mapa aonde os jogadores ser�o teleportados.
	//	coord_x: Coordenada X aonde os jogadores ser�o teleportados. (0: ser� aleat�rio)
	//	coord_y: Coordenada Y aonde os jogadores ser�o teleportados. (0: ser� aleat�rio)
	
	//
	// * Arena PvP
	//
	setarray .@PvPRank_Arenas$[0],
		"Arena Ranqueada", 0, 0, 500, 500, 0, "pvp_r_1-1", 0, 0;		// Para n�o ranqueados
		//"Arena de Bronzes", 1, 4, 500, 500, 0, "pvp_r_2-1", 0, 0,			// Grupo dos Bronze
		//"Arena de Pratas", 5, 8, 500, 500, 0, "pvp_r_3-1", 0, 0,			// Grupo dos Prata
		//"Arena de Ouros", 9, 12, 500, 500, 0, "pvp_r_4-1", 0, 0,			// Grupo dos Ouro
		//"Arena de Platinas", 13, 16, 500, 500, 0, "pvp_r_5-1", 0, 0,		// Grupo de Platina
		//"Arena de Diamantes", 17, 20, 500, 500, 0, "pvp_r_6-1", 0, 0,		// Grupo de Diamantes
		//"Arena de Mestres", 21, 24, 500, 500, 0, "pvp_r_7-1", 0, 0		// Grupo de Mestres
		//;
	
	//
	// * Arena GvG
	//
	//setarray .@GvGRank_Arenas$[0],
	//	"Arena de Iniciantes", 0, 0, 500, 500, 0, "pvp_r_1-2", 0, 0,		// Para n�o ranqueados
	//	"Arena de Bronzes", 1, 4, 500, 500, 0, "pvp_r_2-2", 0, 0,			// Grupo dos Bronze
	//	"Arena de Pratas", 5, 8, 500, 500, 0, "pvp_r_3-2", 0, 0,			// Grupo dos Prata
	//	"Arena de Ouros", 9, 12, 500, 500, 0, "pvp_r_4-2", 0, 0,			// Grupo dos Ouro
	//	"Arena de Platinas", 13, 16, 500, 500, 0, "pvp_r_5-2", 0, 0,		// Grupo de Platina
	//	"Arena de Diamantes", 17, 20, 500, 500, 0, "pvp_r_6-2", 0, 0,		// Grupo de Diamantes
	//	"Arena de Mestres", 21, 24, 500, 500, 0, "pvp_r_7-2", 0, 0		// Grupo de Mestres
	//;
	
	//
	// * Arena Party vs Party
	//
	//setarray .@PvP2Rank_Arenas$[0],
	//	"Arena de Iniciantes", 0, 0, 500, 500, 0, "pvp_r_1-3", 0, 0,		// Para n�o ranqueados
	//	"Arena de Bronzes", 1, 4, 500, 500, 0, "pvp_r_2-3", 0, 0,			// Grupo dos Bronze
	//	"Arena de Pratas", 5, 8, 500, 500, 0, "pvp_r_3-3", 0, 0,			// Grupo dos Prata
	//	"Arena de Ouros",9, 12, 500, 500, 0, "pvp_r_4-3", 0, 0,			// Grupo dos Ouro
	//	"Arena de Platinas", 13, 16, 500, 500, 0, "pvp_r_5-3", 0, 0,		// Grupo de Platina
	//	"Arena de Diamantes", 17, 20, 500, 500, 0, "pvp_r_6-3", 0, 0,		// Grupo de Diamantes
	//	"Arena de Mestres", 21, 24, 1, 500, 0, "pvp_r_7-3", 0, 0		// Grupo de Mestres
	//;
	
	cutin "ein_soldier", 2;
	set .@loop_1,1;
	while(.@loop_1) {
		mes "^8B4513[Porteiro PvP]^000000";
		mes "Bem-vindo a Arena PvP!";
		mes "Em que posso ajudar?";
		next;
		switch( select("- Arenas n�o Ranqueadas.", "^0000FF- Arenas Ranqueadas.^000000", "^FF8C00- Ranking Atual.^000000", "^FF0000- Cancelar.^000000") ) {
			// PvP N�o Ranqueado
			case 1:
				set .@w,0;
				set .@loop_2,1;
				while(.@loop_2) {
					mes "^8B4513[Porteiro PvP]^000000";
					mes "Por favor, selecione o ^0000FFModo de Jogo de partida n�o Ranqueada^000000:";
					next;
					switch( select("- Jogadores vs Jogadores.", "^FF8C00- Voltar.^000000", "^FF0000- Cancelar.^000000") ) {
						case 1:
							set .@w, callsub(sub_Warper, 0, 0, "Arena PvP", .@PvPUnrank_Arenas$, getarraysize(.@PvPUnrank_Arenas$));
							break;
						case 2:
							set .@loop_2,0;
							break;
						default:
							set .@loop_1,0;
							set .@loop_2,0;
							break;
							
					}
					
					if( .@w == 0 ) {
						set .@loop_2,0;
						set .@loop_1,0;
					}
				}
				break;
				
			// Pvp Ranqueado
			case 2:
				set .@w,0;
				set .@loop_2,1;
				while(.@loop_2) {
					mes "^8B4513[Porteiro PvP]^000000";
					mes "Por favor, selecione o ^0000FFModo de Jogo de partida Ranqueada^000000:";
					next;
					switch( select("- Jogadores vs Jogadores.", "^FF8C00- Voltar.^000000", "^FF0000- Cancelar.^000000") ) {
						case 1:
							set .@w, callsub(sub_Warper, 1, 0, "Arena PvP", .@PvPRank_Arenas$, getarraysize(.@PvPRank_Arenas$), 0);
							break;
						case 2:
							set .@loop_2,0;
							break;
						default:
							set .@loop_1,0;
							set .@loop_2,0;
							break;
					}
					
					if( .@w == 0 ) {
						set .@loop_2,0;
						set .@loop_1,0;
					}
				}
				break;
			// Ranking
			case 3:
				freeloop(1);
				set .@loop_2,1;
				deletearray .@char_id;
				query_sql "SELECT `r`.`char_id`, `ch`.`name`, `r`.`rank_id`, `ch`.`class`, `ch`.`base_level`, `ch`.`job_level`, `r`.`points_gained`, `r`.`points_lost`, (`r`.`points_gained`-`r`.`points_lost`) AS `points_ration`, `r`.`kills`, `r`.`deaths`, (`r`.`kills`-`r`.`deaths`) AS `kdr`, `r`.`damage_given`, `r`.`damage_taken`, (`r`.`damage_given`-`r`.`damage_taken`) AS `damage_ration`, `r`.`skill_success`, `r`.`skill_fail`, (`r`.`skill_success`-`r`.`skill_fail`) AS `skill_ration`, `r`.`skill_damage_given`, `r`.`skill_damage_taken`, (`r`.`skill_damage_given`-`r`.`skill_damage_taken`) AS `skill_damage_ration`, `r`.`skill_support_success`, `r`.`skill_support_fail`, (`r`.`skill_support_success`-`r`.`skill_support_fail`) AS `skill_support_ration`, `r`.`heal_hp`, `r`.`heal_sp` FROM `ranked_char` AS `r` LEFT JOIN `char` AS `ch` on `ch`.`char_id`=`r`.`char_id` ORDER BY `points_ration` DESC", .@char_id, .@name$, .@rank_id, .@job, .@base_level, .@job_level, .@points_gained, .@points_lost, .@points_ration, .@kills, .@deaths, .@kdr, .@damage_given, .@damage_taken, .@damage_ration, .@skill_success, .@skill_fail, .@skill_ration, .@skill_damage_given, .@skill_damage_taken, .@skill_damage_ration, .@skill_support_success, .@skill_support_fail, .@skill_support_ration, .@heal_hp, .@heal_sp;
				while(1) {
					set .@rankname$, getrankinfo(.@rank_id[.@i],1);
					mes "^FFA500[Ranking Atual]^000000";
					mes "^0000FFPosi��o:^000000 " + (.@i+1) + "� Colocado";
					mes "^0000FFGrupo de Habilidades:^000000 " + (.@rankname$ == "" ? "Nenhum" : .@rankname$);
					mes "^0000FFJogador:^000000 " + .@name$[.@i];
					mes "^0000FFClasse:^000000 " + jobname(.@job[.@i]);
					mes "^0000FFN�vel:^000000 " + .@base_level[.@i] + "/" + .@job_level[.@i];
					next;
					switch( select(((.@i+1)<getarraysize(.@char_id)?"- Pr�ximo.":""), (.@i>0?"- Anterior.":""), "^0000FF- Estat�sticas.^000000", "^FF0000- Cancelar.^000000") ) {
						case 1:
							set .@i, .@i + 1;
							break;
						case 2:
							set .@i, .@i - 1;
							break;
						case 3:
							mes "^FFA500 ~ Estat�sticas ~ ^000000";
							mes "^0000FFPosi��o:^000000 " + (.@i+1) + "� Colocado";
							mes "^0000FFJogador:^000000 " + .@name$[.@i];
							mes "^0000FFGrupo de Habilidades:^000000 " + (.@rankname$ == "" ? "Nenhum" : .@rankname$);
							mes "^0000FFPontos:^000000 " + .@points_gained[.@i] + "/" + .@points_lost[.@i] + "/" + .@points_ration[.@i];
							mes "^0000FFElimina��es:^000000 " + .@kills[.@i] + "/" + .@deaths[.@i] + "/" + .@kdr[.@i];
							mes "^0000FFDano:^000000 " + .@damage_given[.@i] + "/" + .@damage_taken[.@i] + "/" + .@damage_ration[.@i];
							mes "^0000FFHabilidades:^000000 " + .@skill_success[.@i] + "/" + .@skill_fail[.@i] + "/" + .@skill_ration[.@i];
							mes "^0000FFDano por Habilidades:^000000 " + .@skill_damage_given[.@i] + "/" + .@skill_damage_taken[.@i] + "/" + .@skill_damage_ration[.@i];
							mes "^0000FFSuporte por Habilidades:^000000 " + .@skill_support_success[.@i] + "/" + .@skill_support_fail[.@i] + "/" + .@skill_support_ration[.@i];
							mes "^0000FFRecupera��o de HP:^000000 " + .@heal_hp[.@i];
							mes "^0000FFRecupera��o de SP:^000000 " + .@heal_sp[.@i];
							mes " ";
							mes "^FF0000Legendas:^000000";
							mes "Positivo/Negativo/Propor��o";
							mes "Ganho/Perdido/Propor��o";
							mes "Dado/Tomado/Propor��o";
							mes "Sucesso/Falha/Propor��o";
							next;
							break;
						case 4:
							close2;
	
					}
				}
				freeloop(0);
				close;
				break;
			default:
				set .@loop_1,0;
				break;
		}
	}
	mes "^8B4513[Porteiro PvP]^000000";
	mes "Muito bem, volte quando desejar entrar na ^0000FFArena PvP^000000.";
	close2;
	cutin "", 255;
	end;
	
sub_Warper:
	deletearray .@Arenas$;
	copyarray .@Arenas$, getarg(3), getarg(4);
	set .@loop_3,1;
	while(.@loop_3) {
		set .@n,0;
		set .@buildmenu$, "";
		mes "^8B4513[Porteiro PvP]^000000";
		mes "Escolha a ^0000FF" + getarg(2) + "^000000 que deseja acessar:";
		next;
		set .@c, 0;
		set .@b, getarg(0) ? 3 : 1;
		set .@n, getarg(0) ? 9 : 7;
		for( set .@i,0; .@i < getarraysize(.@Arenas$); set .@i, .@i + .@n ) {	
			set .@buildmenu$, .@buildmenu$+"^0000FF- " + .@Arenas$[.@i] + "^000000, Nv. ";
			
			if( atoi(.@Arenas$[.@i+.@b]) < 1 )
				set .@buildmenu$, .@buildmenu$ + 1;
			else
				set .@buildmenu$, .@buildmenu$ + atoi(.@Arenas$[.@i+.@b]);
				
			set .@buildmenu$, .@buildmenu$ + "~";
			
			set .@max_level, atoi(.@Arenas$[.@i+.@b+1]);
			if( .@max_level < 1 || .@max_level > MAX_LEVEL )
				set .@buildmenu$, .@buildmenu$ + MAX_LEVEL;
			else
				set .@buildmenu$, .@buildmenu$ + atoi(.@Arenas$[.@i+.@b+1]);
				
			set .@buildmenu$, .@buildmenu$+" ^FF0000[" + getmapusers(.@Arenas$[.@i+.@b+3]);
			
			if( atoi(.@Arenas$[.@i+.@b+2]) > 0 )
				set .@buildmenu$, .@buildmenu$ + "/" + atoi(.@Arenas$[.@i+.@b+2]);
						
			set .@buildmenu$, .@buildmenu$ + "]^000000.";
			set .@buildmenu$, .@buildmenu$ + ":";
			set .@c,.@c+1;
		}
		
		set .@buildmenu$, .@buildmenu$ + "^FFA500- Voltar.:^FF0000- Cancelar.^000000";
		set .@choose, select(.@buildmenu$)-1;

		if( .@choose == .@c )
			return 1;
					
		if( .@choose > .@c )
			return 0;

		set .@i, .@choose*.@n;
		set .@name$, .@Arenas$[.@i];
		set .@min_level, atoi(.@Arenas$[.@i+.@b]);
		set .@max_level, atoi(.@Arenas$[.@i+.@b+1]);
		set .@max_players, atoi(.@Arenas$[.@i+.@b+2]);
		set .@mapname$, .@Arenas$[.@i+.@b+3];
		set .@coord_x, atoi(.@Arenas$[.@i+.@b+4]);
		set .@coord_y, atoi(.@Arenas$[.@i+.@b+5]);
		set .@rank_id, getbattleflag("ranked.mode") ? getaccrankdata(0) : getcharrankdata(0);
		
		if( .@min_level < 1 )
			set .@min_level, 1;
		if( .@max_level > MAX_LEVEL )
			set .@max_level, MAX_LEVEL;
		
		mes "^8B4513[Porteiro PvP]^000000";
		//if( BaseLevel < .@min_level || BaseLevel > .@max_level ) {
		//	mes "Somente Jogadores com ^FF0000N�vel " + .@min_level + "~" + .@max_level + "^000000 podem acessar a ^0000FF" + .@name$ + "^000000!";
		//	next;
		//}
		//else 
		if( .@max_players && getmapusers(.@mapname$) >= .@max_players ) {
			mes "A ^0000FF" + .@name$ + "^000000 est� cheia, por favor tente mais tarde.";
			next;
		}
		else if( getarg(1) == 0 && getcharid(1) ) {
			mes "Voc� deve sair do seu ^FF0000Grupo^000000 para entrar na ^0000FF" + .@name$ + "^000000.";
			next;
		}
		else if( getarg(1) == 1 && getcharid(2) <= 0 ) {
			mes "Somente Jogadores com ^FF0000Cl�^000000 podem entrar na ^0000FF" + .@name$ + "^000000.";
			next;
		}
		else if( getarg(1) == 2 && getcharid(1) <= 0 ) {
			mes "Somente Jogadores com ^FF0000Grupo^000000 podem entrar na ^0000FF" + .@name$ + "^000000.";
			next;
		}
		//else if( getarg(0) && !(.@rank_id >= atoi(.@Arenas$[.@i+1]) && .@rank_id <= atoi(.@Arenas$[.@i+2])) ) {
		//	mes "Voc� n�o pode entrar na ^0000FF" + .@name$ + "^000000 com esta qualifica��o.";
		//	next;
		//}
		else {
			mes "Tem certeza que deseja entrar na ^0000FF" + .@name$ + (!getarg(0) ?" n�o ":" ") + "Ranqueada^000000?";
			next;
			switch( select("- Sim, por favor.", "^FFA500- Voltar.", "^FF0000- Cancelar.^000000") ) {
				case 1:
					if( .@max_user && getmapusers(.@mapname$) > .@max_players ) {
						mes "^8B4513[Porteiro PvP]^000000";
						mes "A ^0000FF" + .@name$ + "^000000 est� cheia, por favor tente mais tarde.";
						next;
						break;
					}
					cutin "", 255;
					warp .@mapname$, .@coord_x, .@coord_y;
					end;
				case 2:
					break;
				case 3:
					set .@loop_3,0;
					break;
			}
		}
	}
	return 0;
	
OnInit:
	if( PACKETVER <= 20161228 )
		waitingroom "PvP Ranqueada",0;
		
OnClock0001:
	// Reset de Ranking
	set .@bflag, getbattleflag("ranked.rank_reset");
	if( .@bflag <= 0 )
		end;
		
	set .@tick, gettimetick(2);
	set .@date$, sprintf("%04d-%02d", gettime(DT_YEAR), gettime(DT_MONTH));
	set .@day, gettime(DT_DAYOFMONTH); 
	
	if( $ranked_month_date$ == "" ) {
		set $ranked_month_date$, .@date$;
		set $ranked_week_next, 7;
		
		while($ranked_week_next < .@day) {
			set $ranked_week_next, $ranked_week_next + 7;
		}
		
		if( $ranked_week_next > 31 )
			set $ranked_week_next, 31;
	}
	
	if( .@date$ != $ranked_month_date$ ) {
		if( setrankperiod(1) ) {
			set $ranked_month_date$, .@date$;
			set $ranked_week_next, 7;
		}
	}
	
	if( gettime(DT_DAYOFMONTH) >= $ranked_week_next ) {
		set .@type, .@bflag == 1 ? 1 : 0;
		if( setrankperiod(.@type) ) {
			while($ranked_week_next < .@day) {
				set $ranked_week_next, $ranked_week_next + 7;
			}
			
			if( $ranked_week_next > 31 )
				set $ranked_week_next, 31;
		}
	}
	end;
}

// Mapflags
pvp_r_1-1	mapflag	ranked
pvp_r_1-1	mapflag	pvp
pvp_r_1-1	mapflag	pvp_noparty
pvp_r_1-1	mapflag	pvp_noguild
pvp_r_1-1	mapflag	nowarp
pvp_r_1-1	mapflag	nowarpto
pvp_r_1-1	mapflag	nosave	SavePoint
pvp_r_1-1	mapflag	noteleport
pvp_r_1-1	mapflag	nomemo

pvp_r_2-1	mapflag	ranked
pvp_r_2-1	mapflag	pvp
pvp_r_2-1	mapflag	pvp_noparty
pvp_r_2-1	mapflag	pvp_noguild
pvp_r_2-1	mapflag	nowarp
pvp_r_2-1	mapflag	nowarpto
pvp_r_2-1	mapflag	nosave	SavePoint
pvp_r_2-1	mapflag	noteleport
pvp_r_2-1	mapflag	nomemo

pvp_r_3-1	mapflag	ranked
pvp_r_3-1	mapflag	pvp
pvp_r_3-1	mapflag	pvp_noparty
pvp_r_3-1	mapflag	pvp_noguild
pvp_r_3-1	mapflag	nowarp
pvp_r_3-1	mapflag	nowarpto
pvp_r_3-1	mapflag	nosave	SavePoint
pvp_r_3-1	mapflag	noteleport
pvp_r_3-1	mapflag	nomemo

pvp_r_4-1	mapflag	ranked
pvp_r_4-1	mapflag	pvp
pvp_r_4-1	mapflag	pvp_noparty
pvp_r_4-1	mapflag	pvp_noguild
pvp_r_4-1	mapflag	nowarp
pvp_r_4-1	mapflag	nowarpto
pvp_r_4-1	mapflag	nosave	SavePoint
pvp_r_4-1	mapflag	noteleport
pvp_r_4-1	mapflag	nomemo

pvp_r_5-1	mapflag	ranked
pvp_r_5-1	mapflag	pvp
pvp_r_5-1	mapflag	pvp_noparty
pvp_r_5-1	mapflag	pvp_noguild
pvp_r_5-1	mapflag	nowarp
pvp_r_5-1	mapflag	nowarpto
pvp_r_5-1	mapflag	nosave	SavePoint
pvp_r_5-1	mapflag	noteleport
pvp_r_5-1	mapflag	nomemo

pvp_r_6-1	mapflag	ranked
pvp_r_6-1	mapflag	pvp
pvp_r_6-1	mapflag	pvp_noparty
pvp_r_6-1	mapflag	pvp_noguild
pvp_r_6-1	mapflag	nowarp
pvp_r_6-1	mapflag	nowarpto
pvp_r_6-1	mapflag	nosave	SavePoint
pvp_r_6-1	mapflag	noteleport
pvp_r_6-1	mapflag	nomemo

pvp_r_7-1	mapflag	ranked
pvp_r_7-1	mapflag	pvp
pvp_r_7-1	mapflag	pvp_noparty
pvp_r_7-1	mapflag	pvp_noguild
pvp_r_7-1	mapflag	nowarp
pvp_r_7-1	mapflag	nowarpto
pvp_r_7-1	mapflag	nosave	SavePoint
pvp_r_7-1	mapflag	noteleport
pvp_r_7-1	mapflag	nomemo

pvp_r_1-2	mapflag	ranked
pvp_r_1-2	mapflag	gvg
pvp_r_1-2	mapflag	pvp_noparty
pvp_r_1-2	mapflag	nowarp
pvp_r_1-2	mapflag	nowarpto
pvp_r_1-2	mapflag	nosave	SavePoint
pvp_r_1-2	mapflag	noteleport
pvp_r_1-2	mapflag	nomemo

pvp_r_2-2	mapflag	ranked
pvp_r_2-2	mapflag	gvg
pvp_r_2-2	mapflag	pvp_noparty
pvp_r_2-2	mapflag	nowarp
pvp_r_2-2	mapflag	nowarpto
pvp_r_2-2	mapflag	nosave	SavePoint
pvp_r_2-2	mapflag	noteleport
pvp_r_2-2	mapflag	nomemo

pvp_r_3-2	mapflag	ranked
pvp_r_3-2	mapflag	gvg
pvp_r_3-2	mapflag	pvp_noparty
pvp_r_3-2	mapflag	nowarp
pvp_r_3-2	mapflag	nowarpto
pvp_r_3-2	mapflag	nosave	SavePoint
pvp_r_3-2	mapflag	noteleport
pvp_r_3-2	mapflag	nomemo

pvp_r_4-2	mapflag	ranked
pvp_r_4-2	mapflag	gvg
pvp_r_4-2	mapflag	pvp_noparty
pvp_r_4-2	mapflag	nowarp
pvp_r_4-2	mapflag	nowarpto
pvp_r_4-2	mapflag	nosave	SavePoint
pvp_r_4-2	mapflag	noteleport
pvp_r_4-2	mapflag	nomemo

pvp_r_5-2	mapflag	ranked
pvp_r_5-2	mapflag	gvg
pvp_r_5-2	mapflag	pvp_noparty
pvp_r_5-2	mapflag	nowarp
pvp_r_5-2	mapflag	nowarpto
pvp_r_5-2	mapflag	nosave	SavePoint
pvp_r_5-2	mapflag	noteleport
pvp_r_5-2	mapflag	nomemo

pvp_r_6-2	mapflag	ranked
pvp_r_6-2	mapflag	gvg
pvp_r_6-2	mapflag	pvp_noparty
pvp_r_6-2	mapflag	nowarp
pvp_r_6-2	mapflag	nowarpto
pvp_r_6-2	mapflag	nosave	SavePoint
pvp_r_6-2	mapflag	noteleport
pvp_r_6-2	mapflag	nomemo

pvp_r_7-2	mapflag	ranked
pvp_r_7-2	mapflag	gvg
pvp_r_7-2	mapflag	pvp_noparty
pvp_r_7-2	mapflag	nowarp
pvp_r_7-2	mapflag	nowarpto
pvp_r_7-2	mapflag	nosave	SavePoint
pvp_r_7-2	mapflag	noteleport
pvp_r_7-2	mapflag	nomemo

pvp_r_1-3	mapflag	ranked
pvp_r_1-3	mapflag	pvp
pvp_r_1-3	mapflag	pvp_noguild
pvp_r_1-3	mapflag	nowarp
pvp_r_1-3	mapflag	nowarpto
pvp_r_1-3	mapflag	nosave	SavePoint
pvp_r_1-3	mapflag	noteleport
pvp_r_1-3	mapflag	nomemo

pvp_r_2-3	mapflag	ranked
pvp_r_2-3	mapflag	pvp
pvp_r_2-3	mapflag	pvp_noguild
pvp_r_2-3	mapflag	nowarp
pvp_r_2-3	mapflag	nowarpto
pvp_r_2-3	mapflag	nosave	SavePoint
pvp_r_2-3	mapflag	noteleport
pvp_r_2-3	mapflag	nomemo

pvp_r_3-3	mapflag	ranked
pvp_r_3-3	mapflag	pvp
pvp_r_3-3	mapflag	pvp_noguild
pvp_r_3-3	mapflag	nowarp
pvp_r_3-3	mapflag	nowarpto
pvp_r_3-3	mapflag	nosave	SavePoint
pvp_r_3-3	mapflag	noteleport
pvp_r_3-3	mapflag	nomemo

pvp_r_4-3	mapflag	ranked
pvp_r_4-3	mapflag	pvp
pvp_r_4-3	mapflag	pvp_noguild
pvp_r_4-3	mapflag	nowarp
pvp_r_4-3	mapflag	nowarpto
pvp_r_4-3	mapflag	nosave	SavePoint
pvp_r_4-3	mapflag	noteleport
pvp_r_4-3	mapflag	nomemo

pvp_r_5-3	mapflag	ranked
pvp_r_5-3	mapflag	pvp
pvp_r_5-3	mapflag	pvp_noguild
pvp_r_5-3	mapflag	nowarp
pvp_r_5-3	mapflag	nowarpto
pvp_r_5-3	mapflag	nosave	SavePoint
pvp_r_5-3	mapflag	noteleport
pvp_r_5-3	mapflag	nomemo

pvp_r_6-3	mapflag	ranked
pvp_r_6-3	mapflag	pvp
pvp_r_6-3	mapflag	pvp_noguild
pvp_r_6-3	mapflag	nowarp
pvp_r_6-3	mapflag	nowarpto
pvp_r_6-3	mapflag	nosave	SavePoint
pvp_r_6-3	mapflag	noteleport
pvp_r_6-3	mapflag	nomemo

pvp_r_7-3	mapflag	ranked
pvp_r_7-3	mapflag	pvp
pvp_r_7-3	mapflag	pvp_noguild
pvp_r_7-3	mapflag	nowarp
pvp_r_7-3	mapflag	nowarpto
pvp_r_7-3	mapflag	nosave	SavePoint
pvp_r_7-3	mapflag	noteleport
pvp_r_7-3	mapflag	nomemo

pvp_u_1-1	mapflag	noranked
pvp_u_1-1	mapflag	pvp
pvp_u_1-1	mapflag	pvp_noparty
pvp_u_1-1	mapflag	pvp_noguild
pvp_u_1-1	mapflag	nowarp
pvp_u_1-1	mapflag	nowarpto
pvp_u_1-1	mapflag	nosave	SavePoint
pvp_u_1-1	mapflag	noteleport
pvp_u_1-1	mapflag	nomemo

pvp_u_2-1	mapflag	noranked
pvp_u_2-1	mapflag	pvp
pvp_u_2-1	mapflag	pvp_noparty
pvp_u_2-1	mapflag	pvp_noguild
pvp_u_2-1	mapflag	nowarp
pvp_u_2-1	mapflag	nowarpto
pvp_u_2-1	mapflag	nosave	SavePoint
pvp_u_2-1	mapflag	noteleport
pvp_u_2-1	mapflag	nomemo

pvp_u_3-1	mapflag	noranked
pvp_u_3-1	mapflag	pvp
pvp_u_3-1	mapflag	pvp_noparty
pvp_u_3-1	mapflag	pvp_noguild
pvp_u_3-1	mapflag	nowarp
pvp_u_3-1	mapflag	nowarpto
pvp_u_3-1	mapflag	nosave	SavePoint
pvp_u_3-1	mapflag	noteleport
pvp_u_3-1	mapflag	nomemo

pvp_u_4-1	mapflag	noranked
pvp_u_4-1	mapflag	pvp
pvp_u_4-1	mapflag	pvp_noparty
pvp_u_4-1	mapflag	pvp_noguild
pvp_u_4-1	mapflag	nowarp
pvp_u_4-1	mapflag	nowarpto
pvp_u_4-1	mapflag	nosave	SavePoint
pvp_u_4-1	mapflag	noteleport
pvp_u_4-1	mapflag	nomemo

pvp_u_1-2	mapflag	noranked
pvp_u_1-2	mapflag	gvg
pvp_u_1-2	mapflag	pvp_noparty
pvp_u_1-2	mapflag	nowarp
pvp_u_1-2	mapflag	nowarpto
pvp_u_1-2	mapflag	nosave	SavePoint
pvp_u_1-2	mapflag	noteleport
pvp_u_1-2	mapflag	nomemo

pvp_u_2-2	mapflag	noranked
pvp_u_2-2	mapflag	gvg
pvp_u_2-2	mapflag	pvp_noparty
pvp_u_2-2	mapflag	nowarp
pvp_u_2-2	mapflag	nowarpto
pvp_u_2-2	mapflag	nosave	SavePoint
pvp_u_2-2	mapflag	noteleport
pvp_u_2-2	mapflag	nomemo

pvp_u_3-2	mapflag	noranked
pvp_u_3-2	mapflag	gvg
pvp_u_3-2	mapflag	pvp_noparty
pvp_u_3-2	mapflag	nowarp
pvp_u_3-2	mapflag	nowarpto
pvp_u_3-2	mapflag	nosave	SavePoint
pvp_u_3-2	mapflag	noteleport
pvp_u_3-2	mapflag	nomemo

pvp_u_4-2	mapflag	noranked
pvp_u_4-2	mapflag	gvg
pvp_u_4-2	mapflag	pvp_noparty
pvp_u_4-2	mapflag	nowarp
pvp_u_4-2	mapflag	nowarpto
pvp_u_4-2	mapflag	nosave	SavePoint
pvp_u_4-2	mapflag	noteleport
pvp_u_4-2	mapflag	nomemo

pvp_u_1-3	mapflag	noranked
pvp_u_1-3	mapflag	pvp
pvp_u_1-3	mapflag	pvp_noguild
pvp_u_1-3	mapflag	nowarp
pvp_u_1-3	mapflag	nowarpto
pvp_u_1-3	mapflag	nosave	SavePoint
pvp_u_1-3	mapflag	noteleport
pvp_u_1-3	mapflag	nomemo

pvp_u_2-3	mapflag	noranked
pvp_u_2-3	mapflag	pvp
pvp_u_2-3	mapflag	pvp_noguild
pvp_u_2-3	mapflag	nowarp
pvp_u_2-3	mapflag	nowarpto
pvp_u_2-3	mapflag	nosave	SavePoint
pvp_u_2-3	mapflag	noteleport
pvp_u_2-3	mapflag	nomemo

pvp_u_3-3	mapflag	noranked
pvp_u_3-3	mapflag	pvp
pvp_u_3-3	mapflag	pvp_noguild
pvp_u_3-3	mapflag	nowarp
pvp_u_3-3	mapflag	nowarpto
pvp_u_3-3	mapflag	nosave	SavePoint
pvp_u_3-3	mapflag	noteleport
pvp_u_3-3	mapflag	nomemo

pvp_u_4-3	mapflag	noranked
pvp_u_4-3	mapflag	pvp
pvp_u_4-3	mapflag	pvp_noguild
pvp_u_4-3	mapflag	nowarp
pvp_u_4-3	mapflag	nowarpto
pvp_u_4-3	mapflag	nosave	SavePoint
pvp_u_4-3	mapflag	noteleport
pvp_u_4-3	mapflag	nomemo