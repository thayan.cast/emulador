quiz_00,50,35,5	script	Retirador de Items	4_F_JP_RINNE,{

// === Configura��es ========================================= //
set $namenpc$, "[^ff0033 Npc Name ^000000]";                    // Nome do NPC.
set $namechar$, strcharinfo(0);                                 // Nome do Char.
set $@GMLevel, 60;                                              // Nome do Char.

// === Come�o da Script ===================================== //
if(getgroupid() >= $@GMLevel) {
L_menu:
mes $namenpc$;
mes "Ol� "+$namechar$+" ";
mes "Em que posso ajudar ?";
next;
switch(select("^CC00FFRetirar Item:Cancelar")) {

	case 1:
	mes $namenpc$;
	mes "Digite o ID do Item";
	input .@iditem$;
	mes $namenpc$;
	mes "Voc� digitou ^ff0033"+.@iditem$+"^000000 est� correto ?";
	switch(select("Est� Correto:N�o, desejo reescrever")){
			
			case 1:
			mes $namenpc$;
			mes "Digite a quantidade";
			input .@quantidadeitems$;
			next;
			mes "Perfeito, agora digite o motivo de estar retirando este item !";
			next;
			input .@motivo$;
			mes "Ent�o voc� est� solicitando ^ff0033"+.@quantidadeitems$+"^000000 do item ^ff0033"+.@iditem$+"^000000, com o motivo:  ";
			mes "^ff0033"+.@motivo$+"^000000";
				switch(select("Est� Correto:N�o, desejo reescrever")){
				
				case 1:
				if(checkweight(atoi(.@iditem$),atoi(.@quantidadeitems$))) {

                set $querySet$, query_sql("INSERT INTO `retirador_items` (`nome_char`, `id_item`, `quantidade_item`, `motivo`) VALUES ('"+$namechar$+"', "+atoi(.@iditem$)+", "+atoi(.@quantidadeitems$)+", '"+.@motivo$+"');");
                if (atoi($querySet$) == 0) {
                getitem atoi(.@iditem$),atoi(.@quantidadeitems$);
				mes $namenpc$;
				mes "Estou lhe entregando";
				next;
				goto L_menu;
				close;
				} else {
				mes "Ocorreu um erro, reveja com o ADM";
                next;
                close;
                end;
				}
                } else {
                mes "^FF0000 Voce pediu a quantidade de items acima do peso.^000000";
                next;
                goto L_menu;
                close;
                }
							
				case 2:
				mes $namenpc$;
				mes "Ok, retornarei ao �ltimo menu ent�o !";
				next;
				goto L_menu;
				close;
				
			}
				
			case 2:
			mes $namenpc$;
			mes "Ok, retornarei ao �ltimo menu ent�o !";
			next;
			goto L_menu;
			close;
			}
	
	case 2:
	mes $namenpc$;
	mes "Certo, volte quando precisar de meus servi�os.";
	cutin "linne.bmp",255;
	close;
	}
    } else {
      warp "prontera",156,191;
    }
}