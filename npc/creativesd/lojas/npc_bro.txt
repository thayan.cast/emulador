prontera,141,207,4	script	Quest's bRO	94,{
OnNpcQuest:
cutin "kafra_08",2;
function Add; function Chk; function Slot; function A_An;
		mes "^339966[ Quest's bRO's ]^000000";
		mes (gettime(3)>= 6&&gettime(3)<= 12?"Bom-dia":(gettime(3)>=13&&gettime(3)<=18?"Boa-tarde":"Boa-noite"))+", ^008aff"+strcharinfo(0)+"^000000 !";
		mes "Aqui no ^008aff RagnaZero ^000000 temos um novo sistema de quests que funciona como uma loja que pede items.";
		next;
		mes "^339966[ Quest's bRO's ]^000000";
		mes "Ent�o, qual tipo de item voc� quer fazer?";
		next;
	if(.Shops$ != "") set .@i,1;
	else {
		set .@menu$,"";
		for(set .@i,1; .@i<=getarraysize(.Shops$); set .@i,.@i+1)
			set .@menu$, .@menu$+.Shops$[.@i]+":";
		set .@i, select(.@menu$); }
	dispbottom "Selecione um item de cada vez.";
	callshop "bshop"+.@i,1;
	npcshopattach "bshop"+.@i;
	end;
function Add {
	if (getitemname(getarg(1))=="null") {
		debugmes "Quest reward #"+getarg(1)+" invalid (skipped)."; return; }
	for(set .@n,5; .@n<127; set .@n,.@n+2) {
		if (!getarg(.@n,0)) break;
		if (getitemname(getarg(.@n))=="null") {
			debugmes "Quest requirement #"+getarg(.@n)+" invalid (skipped)."; return; } }
	for(set .@i,2; .@i<.@n; set .@i,.@i+1)
		set getd(".q_"+getarg(1)+"["+(.@i-2)+"]"), getarg(.@i);
	npcshopadditem "bshop"+getarg(0),getarg(1),((.ShowZeny)?getarg(3):0);
	return; }
function Chk {
	if (getarg(0)<getarg(1)) { set @qe0,1; return "^FF0000"; }
	else return "^00FF00"; }
function Slot {
	set .@s$,getitemname(getarg(0));
	switch(.ShowSlot){
		case 1: if (!getitemslots(getarg(0))) return .@s$;
		case 2: if (getiteminfo(getarg(0),11)>0) return .@s$+" ["+getitemslots(getarg(0))+"]";
		default: return .@s$; } }
function A_An {
	setarray .@A$[0],"a","e","i","o","u";
	set .@B$, "_"+getarg(0);
	for(set .@i,0; .@i<5; set .@i,.@i+1)
		if (compare(.@B$,"_"+.@A$[.@i])) return "an "+getarg(0);
	return "a "+getarg(0); }
OnBuyItem:
	set .@q[0],@bought_nameid;
	copyarray .@q[1],getd(".q_"+@bought_nameid+"[0]"),getarraysize(getd(".q_"+@bought_nameid+"[0]"));
	if (!.@q[1]) { message strcharinfo(0),"Ocorreu um erro."; end; }
	mes "^339966[ Quest's bRO's ]^000000";
	mes "Recompensa: ^0055FF"+((.@q[1]>1)?.@q[1]+"x ":"")+Slot(.@q[0])+"^000000";
	mes "Requerimentos:";
	if (.@q[2]) mes " > "+Chk(Zeny,.@q[2])+.@q[2]+" Zeny^000000";
	if (.@q[3]) mes " > "+Chk(getd(.Points$[0]),.@q[3])+.@q[3]+" "+.Points$[1]+" ("+getd(.Points$[0])+"/"+.@q[3]+")^000000";
	if (.@q[4]) for(set .@i,4; .@i<getarraysize(.@q); set .@i,.@i+2)
		mes " > "+Chk(countitem(.@q[.@i]),.@q[.@i+1])+((.DisplayID)?"["+.@q[.@i]+"] ":"")+Slot(.@q[.@i])+" ("+countitem(.@q[.@i])+"/"+.@q[.@i+1]+")^000000";
	next;
	set @qe1, getiteminfo(.@q[0],5); set @qe2, getiteminfo(.@q[0],11);
	addtimer 1000, strnpcinfo(1)+"::OnEnd";
	while(1){
		switch(select("^008000[�]^000000 Fazer ^0055FF"+getitemname(.@q[0])+"^000000:"+((((@qe1&1) || (@qe1&256) || (@qe1&512)) && @qe2>0 && !@qe6)?"^008000[�]^000000 Experimentar Item.":"")+":^777777[�] Cancel.^000000")) {
			case 1:
				if (@qe0) { 
					mes "[ ^008000Quest Shop^000000 ]";
					mes "Voc� est� esquecendo um ou mais requisitos da quest.";
					close; }
				if (!checkweight(.@q[0],.@q[1])) {
					mes "^339966[ Quest's bRO's ]^000000";
					mes "^FF0000Voc� precisa "+(((.@q[1]*getiteminfo(.@q[0],6))+Weight-MaxWeight)/10)+" capacidade adicional de peso para completar esta compra.^000000";
					close; }
				if (.@q[2]) set Zeny, Zeny-.@q[2];
				if (.@q[3]) setd .Points$[0], getd(.Points$[0])-.@q[3];
				if (.@q[4]) for(set .@i,4; .@i<getarraysize(.@q); set .@i,.@i+2)
					delitem .@q[.@i],.@q[.@i+1];
				getitem .@q[0],.@q[1];
				if (.Announce) announce strcharinfo(0)+" acabou de realizar a quest do item: ["+(getitemname(.@q[0]))+"]!",0;
				specialeffect2 699;
				close;
			case 2:
				set @qe3, getlook(3); set @qe4, getlook(4); set @qe5, getlook(5);
				if (@qe1&1) atcommand "@changelook 3 "+@qe2;
				if (@qe1&256) atcommand "@changelook 1 "+@qe2;
				if (@qe1&512) atcommand "@changelook 2 "+@qe2;
				set @qe6,1;
				break;
			case 3:
				close; } }
OnEnd:
	if (@qe6) { atcommand "@changelook 3 "+@qe3; atcommand "@changelook 1 "+@qe4; atcommand "@changelook 2 "+@qe5; }
	for(set .@i,0; .@i<7; set .@i,.@i+1) setd "@qe"+.@i,0;
	end;
OnPCLoadMapEvent:
	if( strcharinfo(3) == strnpcinfo(4) )
		showevent 1,0;
	end;
OnInit:
	freeloop(1);
// --------------------- Config ---------------------
// Custom points, if needed: "<variable>","<name to display>"
	setarray .Points$[0],"#CASHPOINTS","Cash Points";
	set .Announce,1;	// Anunciar ao completar a quest? (1: sim / 0: n�o)
	set .ShowSlot,2;	// Mostrar slot dos itens? (2: Todos os equipamentos / 1: Se a quantidade > 0 / 0: Nunca)
	set .DisplayID,1;	// Mostrar ID dos itens? (1: sim / 0: n�o)
	set .ShowZeny,0;	// Mostrar quantidade de Zeny? (1: sim / 0: n�o)
// Shop categories, if needed: "<Shop 1>","<Shop 2>"{,...};
// Duplicate dummy data for any additional shops (bottom of script).
// If no categories, use the second line instead (remove //).
 
	setarray .Shops$[1],"^339966[�]^000000 Quest [^ff0000BRO^000000].";
	// set .Shops$,"n/a";
// Add(<shop number>,<reward ID>,<reward amount>,<Zeny cost>,<point cost>,
//     <required item ID>,<required item amount>{,...});
// Shop number corresponds with order above (default is 1).
// Note: Do NOT use a reward item more than once!
//topo
	Add(1,5170,1,0,0,5172,1,7063,100,982,1);
	Add(1,5086,1,0,0,1095,3000,2288,1);
	Add(1,2284,1,0,0,923,20);
	Add(1,5110,1,0,0,526,2,7270,1,941,1,10004,1);
	Add(1,2296,1,50000,0,2243,1,999,100);
	Add(1,5057,1,10000,0,2213,1,983,1,914,200);
	Add(1,5016,1,0,0,1030,10);
	Add(1,2214,1,0,0,949,100,706,1,722,1,2213,1);
	Add(1,5107,1,0,0,519,50,7031,50,548,50,539,50);
	Add(1,5082,1,0,0,921,300);
	Add(1,2283,1,5000,0,724,1,5001,1,949,200);
	Add(1,5069,1,0,0,1022,999);
	Add(1,5001,1,0,0,999,40,984,1,970,1,1003,1);
	Add(1,5070,1,0,0,7216,300,7097,300,2211,1,982,1);
	Add(1,5071,1,10000,0,5010,1,5049,1,7101,10);
	Add(1,5073,1,0,0,2285,1,1550,1);
	Add(1,2278,1,0,0,705,10,909,10,914,10);
	Add(1,5117,1,50000,0,731,10,748,2,982,1);
	Add(1,5094,1,0,0,909,10000,931,10000,968,100);
	Add(1,5004,1,0,0,701,5);
	Add(1,5012,1,0,0,710,1,703,1,704,1,708,1);
	Add(1,2293,1,0,0,1049,4);
	Add(1,5109,1,50000,0,10015,1,10007,1,975,1,5032,1);
	Add(1,5083,1,0,0,2244,1,2209,1,10007,1);
	Add(1,5108,1,1887,0,7301,1887,5120,1,611,10);
	Add(1,5078,1,0,0,5033,1,5064,1);
	Add(1,2272,1,91100,0,1019,50,983,1);
	Add(1,5059,1,0,0,5030,1,7213,100,7217,100,7161,300);
	Add(1,5077,1,0,0,2278,1,975,1);
	Add(1,2292,1,2000,0,999,50);
	Add(1,5115,1,0,0,983,1,7267,999,749,1);
	Add(1,5121,1,0,0,7315,369,7263,1,660,1,7099,30);
	Add(1,5074,1,20000,0,2286,1,2254,1);
	Add(1,5068,1,20000,0,2286,1,2255,1);
	Add(1,5025,1,0,0,2229,1,2254,1,7036,5);
	Add(1,5038,1,0,0,1038,600,7048,40);
	Add(1,5091,1,20000,0,10016,1,714,1,969,3);
	Add(1,5080,1,20000,0,10006,1,714,1,969,3);
	Add(1,5081,1,40000,0,2249,1,714,1,969,3);
	Add(1,5079,1,0,0,2294,1,7220,400);
	Add(1,5063,1,0,0,970,1,930,500);
	Add(1,5061,1,0,0,2269,1,999,10);
	Add(1,5042,1,0,0,10007,1,968,50);
	Add(1,5048,1,0,0,5041,1,999,10);
	Add(1,5047,1,0,0,2271,1,975,1);
	Add(1,5041,1,0,0,7013,1200);
	Add(1,5040,1,0,0,7047,100);
	Add(1,5024,1,0,0,529,10,530,10,539,20,999,10,538,15);
	Add(1,5028,1,0,0,2279,1,7035,50,526,100);
	Add(1,5026,1,0,0,1036,450,949,330,539,120,982,1);
	Add(1,5036,1,0,0,2608,1,7069,500);
	Add(1,5034,1,0,0,2233,1,746,20);
	Add(1,5049,1,0,0,1099,1500);
	Add(1,5052,1,0,0,2211,1,978,1,7003,300);
	Add(1,2273,1,3500,0,2275,1,998,50,733,1);
	Add(1,5018,1,500,0,2247,1,916,300);
	Add(1,2281,1,5000,0,998,20,707,1);
	Add(1,2280,1,10000,0,1019,120);
	Add(1,5058,1,0,0,2233,1,983,1,7206,300,7030,1);
	Add(1,5064,1,0,0,945,600,7030,1);
	Add(1,5084,1,0,0,1026,1000,7065,100,945,10,7030,1);
	Add(1,5065,1,0,0,624,1,959,300,551,50,1023,1,938,100,7030,1);
	Add(1,5032,1,0,0,1059,250,2221,1,2227,1,7063,600);
	Add(1,5027,1,0,0,2252,1,1036,400,7001,50,4052,1);
	Add(1,5045,1,0,0,2252,1,1054,450,943,1200 );
	Add(1,5031,1,0,0,5009,1,5028,1,747,1,999,25);
	Add(1,5023,1,0,0,1059,150,907,100,978,1);
	Add(1,5021,1,0,0,2233,1,969,1,999,20,949,80,938,800);
	Add(1,5043,1,0,0,2281,1,1048,50);
	Add(1,5033,1,0,0,1036,20,2213,1,7065,300,7012,200);
	Add(1,5039,1,0,0,7030,50,978,1,5015,1);
	Add(1,5029,1,0,0,7068,300,7033,850,1015,1);
	Add(1,5050,1,0,0,5037,1,7064,500);
	Add(1,5060,1,0,0,2236,1,7151,100,7111,100);
	Add(1,5062,1,0,0,2280,1,7197,300,7150,300);
	Add(1,5075,1,0,0,2248,1,7030,108,7194,108,7120,4);
	Add(1,5067,1,0,0,5062,1,952,50,1907,1);
	Add(1,5076,1,0,0,2226,1,7038,500);
	
// --------------------------------------------------
	freeloop(0);
	for(set .@i,1; .@i<=getarraysize(.Shops$); set .@i,.@i+1)
		npcshopdelitem "bshop"+.@i,909;
		end;
}
// -------- Dummy data (duplicate as needed) --------
-	shop	bshop1	-1,909:-1
//prontera,68,56,5	duplicate(CentralQuest)	Quest's bRO#1	94