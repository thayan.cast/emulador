//*.-----------------------------------------------------------------.
// .    ____                          __                             .
// .   /\  _`\                       /\ \__  __                      .
// .   \ \ \/\_\  _ __    __     __  \ \ ,_\/\_\  __  __     __      .
// .    \ \ \/_/_/\`'__\/'__`\ /'__`\ \ \ \/\/\ \/\ \/\ \  /'__`\    .
// .     \ \ \s\ \ \ \//\  __//\ \d\.\_\ \ \_\ \ \ \ \_/ |/\  __/    .
// .      \ \____/\ \_\\ \____\ \__/.\_\\ \__\\ \_\ \___/ \ \____\   .
// .       \/___/  \/_/ \/____/\/__/\/_/ \/__/ \/_/\/__/   \/____/   .
// .                                                                 .
// .          2014~2018 � Creative Services and Development          .
// .                      www.creativesd.com.br                      .
// .-----------------------------------------------------------------.
// .                     Banco de Dados de Rank                      .
// .-----------------------------------------------------------------.
// . Estrutura:                                                      .
// .	ID,Name,Points,Next_Rank,Emblem,{ Aura }                     .
// .                                                                 .
// .	ID: Identifica��o.                                           .
// .	Name: Nome do Rank.                                          .
// .	Points: Pontos Necess�rio.                                   .
// .	Emblem: Nome do arquivo de Emblema.                          .
// .	Aura: Id das Auras, exemplo #id1 #id2 #id3                   .
// .              Os Efeitos ficam listado dentro da pasta           .
// .              doc/effect_list.txt                                .
// .                                                                 .
// .  Aten��o: O pr�ximo rank ser� designado ao pr�ximo ID mesmo n�o .
// .           estando na ordem.                                     .
// *-----------------------------------------------------------------*
1,Bronze I,200,bronze_1,{ #239 }
2,Bronze II,200,bronze_2,{ #239 #239 }
3,Bronze III,200,bronze_3,{ #239 #239 #239 }
4,Bronze Elite,200,bronze_4,{ #239 #239 #418 #418 #418 }
5,Prata I,200,silver_1,{ #240 }
6,Prata II,200,silver_2,{ #240 #240 }
7,Prata III,200,silver_3,{ #240 #240 #240 }
8,Prata Elite,200,silver_4,{ #254 #254 #240 #240 }
9,Ouro I,200,gold_1,{ #241 }
10,Ouro II,200,gold_2,{ #241 #241 }
11,Ouro III,200,gold_3,{ #241 #241 #241 }
12,Ouro Elite,200,gold_4,{ #485 #485 #241 #241 #241 }
13,Platina I,200,platinum_1,{ #200 }
14,Platina II,200,platinum_2,{ #200 #200 }
15,Platina III,200,platinum_3,{ #200 #200 #202 }
16,Platina Elite,200,platinum_4,{ #200 #201 #202 #203 #203 #203 }
17,Diamente I,200,diamond_1,{ #620 }
18,Diamente II,200,diamond_2,{ #620 #620 }
19,Diamente III,200,diamond_3,{ #620 #620 #620 }
20,Diamente Elite,200,diamond_4,{ #242 #620 #203 #620 #203 }
21,Mestre I,200,master_1,{ #822 }
22,Mestre II,200,master_2,{ #822 #397 }
23,Mestre III,200,master_3,{ #397 #822 #397 }
24,Supremo,200,master_4,{ #397 #822 #397 #203 #254 #418 }