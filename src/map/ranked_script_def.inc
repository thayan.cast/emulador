// © Creative Services and Development
// Site Oficial: www.creativesd.com.br
// Termos de Contrato e Autoria em: http://creativesd.com.br/?p=termos

	BUILDIN_DEF(getrankinfo, "ii"),
	BUILDIN_DEF2(getrankinfo, "getrankinfo2", "ii"),
	BUILDIN_DEF(getcharrankdata, "i?"),
	BUILDIN_DEF(setcharrankdata, "i?"),
	BUILDIN_DEF(getaccrankdata, "i?"),
	BUILDIN_DEF(setaccrankdata, "i?"),
	BUILDIN_DEF(setrankpoints, "i??"),
	BUILDIN_DEF(setrank, "i??"),
	BUILDIN_DEF(setrankperiod, "i?"),
