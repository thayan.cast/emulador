// © Creative Services and Development
// Site Oficial: www.creativesd.com.br
// Termos de Contrato e Autoria em: http://creativesd.com.br/?p=termos

	// Ranked System
	MF_RANKED,
	MF_NORANKED,
	MF_RANKED_PK_GAINED,
	MF_RANKED_PK_LOST,
	MF_RANKED_PD_GIVEN,
	MF_RANKED_PD_GAINED,
	MF_RANKED_PD_TAKEN,
	MF_RANKED_PD_LOST,
	MF_RANKED_SD_GIVEN,
	MF_RANKED_SD_GAINED,
	MF_RANKED_SD_TAKEN,
	MF_RANKED_SD_LOST,
	MF_RANKED_SS_SUCCESS,
	MF_RANKED_SS_GAINED,
	MF_RANKED_SS_FAIL,
	MF_RANKED_SS_LOST,