// © Creative Services and Development
// Site Oficial: www.creativesd.com.br
// Termos de Contrato e Autoria em: http://creativesd.com.br/?p=termos

	// Ranked System
	export_constant(MF_RANKED);
	export_constant(MF_NORANKED);
	export_constant(MF_RANKED_PK_GAINED);
	export_constant(MF_RANKED_PK_LOST);
	export_constant(MF_RANKED_PD_GIVEN);
	export_constant(MF_RANKED_PD_GAINED);
	export_constant(MF_RANKED_PD_TAKEN);
	export_constant(MF_RANKED_PD_LOST);
	export_constant(MF_RANKED_SD_GIVEN);
	export_constant(MF_RANKED_SD_GAINED);
	export_constant(MF_RANKED_SD_TAKEN);
	export_constant(MF_RANKED_SD_LOST);
	export_constant(MF_RANKED_SS_GAINED);
	export_constant(MF_RANKED_SS_LOST);