// © Creative Services and Development
// Site Oficial: www.creativesd.com.br
// Termos de Contrato e Autoria em: http://creativesd.com.br/?p=termos

BUILDIN_FUNC(getrankinfo)
{
	struct ranked_data *rank;
	int rank_id = script_getnum(st,2),
		type = script_getnum(st,3);

	if( (rank = ranked_search(rank_id)) == NULL )
	{
		if( type == 1 )
			script_pushstrcopy(st,"");
		else
			script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	switch(type)
	{
		case 0:
			script_pushint(st, rank->points);
			break;
		case 1:
			script_pushstrcopy(st,rank->g->name);
			break;
		case 2:
			script_pushint(st, rank->back_id);
			break;
		case 3:
			script_pushint(st, rank->next_id);
			break;
		default:
			ShowWarning("buildin_getrankinfo: invalid type '%d'.\n", type);
			script_pushint(st,0);
			break;
	}
	return SCRIPT_CMD_SUCCESS;
}

BUILDIN_FUNC(getcharrankdata)
{
	TBL_PC *sd;
	int type = script_getnum(st,2), val = 0;

	if( !script_mapid2sd(3,sd) ) {
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	switch(type)
	{
		case 0:
			val = sd->status.ranked_char.id;
			break;
		case 1:
			val = sd->status.ranked_char.points.current;
			break;
		case 2:
			val = sd->status.ranked_char.points.gained;
			break;
		case 3:
			val = sd->status.ranked_char.points.lost;
			break;
		case 4:
			val = sd->status.ranked_char.player.kills;
			break;
		case 5:
			val = sd->status.ranked_char.player.deaths;
			break;
		case 6:
			val = sd->status.ranked_char.player.damage_given;
			break;
		case 7:
			val = sd->status.ranked_char.player.damage_taken;
			break;
		case 8:
			val = sd->status.ranked_char.skill.success;
			break;
		case 9:
			val = sd->status.ranked_char.skill.fail;
			break;
		case 10:
			val = sd->status.ranked_char.skill.support_success;
			break;
		case 11:
			val = sd->status.ranked_char.skill.support_fail;
			break;
		case 12:
			val = sd->status.ranked_char.skill.damage_given;
			break;
		case 13:
			val = sd->status.ranked_char.skill.damage_taken;
			break;
		case 14:
			val = sd->status.ranked_char.skill.damage_given;
			break;
		default:
			ShowWarning("buildin_getcharrankinfo: invalid type '%d'.\n", type);
			script_pushint(st,0);
			return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st,val);
	return SCRIPT_CMD_SUCCESS;
}

BUILDIN_FUNC(setcharrankdata)
{
	TBL_PC *sd;
	int rank_id = 0;
	int type = script_getnum(st,2), val = script_getnum(st,3);

	if( !script_mapid2sd(4,sd) ) {
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	switch(type)
	{
		case 1:
			sd->status.ranked_char.points.gained = val;
			break;
		case 2:
			sd->status.ranked_char.points.lost = val;
			break;
		case 3:
			sd->status.ranked_char.player.kills = val;
			break;
		case 4:
			sd->status.ranked_char.player.deaths = val;
			break;
		case 5:
			sd->status.ranked_char.player.damage_given = val;
			break;
		case 6:
			sd->status.ranked_char.player.damage_taken = val;
			break;
		case 7:
			sd->status.ranked_char.skill.success = val;
			break;
		case 8:
			sd->status.ranked_char.skill.fail = val;
			break;
		case 9:
			sd->status.ranked_char.skill.support_success = val;
			break;
		case 10:
			sd->status.ranked_char.skill.support_fail = val;
			break;
		case 11:
			sd->status.ranked_char.skill.damage_given = val;
			break;
		case 12:
			sd->status.ranked_char.skill.damage_taken = val;
			break;
		case 13:
			sd->status.ranked_char.skill.damage_given = val;
			break;
		default:
			ShowWarning("buildin_setcharrankdata: invalid type '%d'.\n", type);
			script_pushint(st,0);
			return SCRIPT_CMD_SUCCESS;
	}
	script_pushint(st,val);
	return SCRIPT_CMD_SUCCESS;
}

BUILDIN_FUNC(getaccrankdata)
{
	TBL_PC *sd;
	int type = script_getnum(st,2), val = 0;

	if( !script_mapid2sd(3,sd) ) {
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	switch(type)
	{
		case 0:
			val = sd->status.ranked_account.id;
			break;
		case 1:
			val = sd->status.ranked_account.points.current;
			break;
		case 2:
			val = sd->status.ranked_account.points.gained;
			break;
		case 3:
			val = sd->status.ranked_account.points.lost;
			break;
		case 4:
			val = sd->status.ranked_account.player.kills;
			break;
		case 5:
			val = sd->status.ranked_account.player.deaths;
			break;
		case 6:
			val = sd->status.ranked_account.player.damage_given;
			break;
		case 7:
			val = sd->status.ranked_account.player.damage_taken;
			break;
		case 8:
			val = sd->status.ranked_account.skill.success;
			break;
		case 9:
			val = sd->status.ranked_account.skill.fail;
			break;
		case 10:
			val = sd->status.ranked_account.skill.support_success;
			break;
		case 11:
			val = sd->status.ranked_account.skill.support_fail;
			break;
		case 12:
			val = sd->status.ranked_account.skill.damage_given;
			break;
		case 13:
			val = sd->status.ranked_account.skill.damage_taken;
			break;
		case 14:
			val = sd->status.ranked_account.skill.damage_given;
			break;
		default:
			ShowWarning("buildin_getaccrankinfo: invalid type '%d'.\n", type);
			script_pushint(st,0);
			return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st,val);
	return SCRIPT_CMD_SUCCESS;
}

BUILDIN_FUNC(setaccrankdata)
{
	TBL_PC *sd;
	int type = script_getnum(st,2), val = script_getnum(st,3);

	if( !script_mapid2sd(4,sd) ) {
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	switch(type)
	{
		case 1:
			sd->status.ranked_account.points.gained = val;
			break;
		case 2:
			sd->status.ranked_account.points.lost = val;
			break;
		case 3:
			sd->status.ranked_account.player.kills = val;
			break;
		case 4:
			sd->status.ranked_account.player.deaths = val;
			break;
		case 5:
			sd->status.ranked_account.player.damage_given = val;
			break;
		case 6:
			sd->status.ranked_account.player.damage_taken = val;
			break;
		case 7:
			sd->status.ranked_account.skill.success = val;
			break;
		case 8:
			sd->status.ranked_account.skill.fail = val;
			break;
		case 9:
			sd->status.ranked_account.skill.support_success = val;
			break;
		case 10:
			sd->status.ranked_account.skill.support_fail = val;
			break;
		case 11:
			sd->status.ranked_account.skill.damage_given = val;
			break;
		case 12:
			sd->status.ranked_account.skill.damage_taken = val;
			break;
		case 13:
			sd->status.ranked_account.skill.damage_given = val;
			break;
		default:
			ShowWarning("buildin_setaccrankdata: invalid type '%d'.\n", type);
			script_pushint(st,0);
			return SCRIPT_CMD_SUCCESS;
	}
	script_pushint(st,val);
	return SCRIPT_CMD_SUCCESS;
}

BUILDIN_FUNC(setrankpoints)
{
	TBL_PC *sd;
	int points, flag, type = RANK_BOTH;

	points = script_getnum(st,2);
	if( script_hasdata(st,3) )
	{
		type = script_getnum(st,3);
		if( type < RANK_CHAR || type > RANK_BOTH )
		{
			ShowError("buildin_setrankpoints: Invalid type %d.\n", type);
			st->state = END;
			return SCRIPT_CMD_FAILURE;
		}
	}

	if( script_mapid2sd(4,sd) ) {
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	if( points >= 0 )
		flag = ranked_points_gained(sd,points,RANK_P_OTHERS,(enum ranked_update)type);
	else
		flag = ranked_points_lost(sd,points,RANK_P_OTHERS,(enum ranked_update)type);
	script_pushint(st,flag);
	return SCRIPT_CMD_SUCCESS;
}

BUILDIN_FUNC(setrank)
{
	TBL_PC *sd;
	struct ranked_data *rank;
	int rank_id = script_getnum(st,2), type = RANK_BOTH;
	
	if( script_hasdata(st,3) )
	{
		type = script_getnum(st,3);
		if( type < RANK_CHAR || type > RANK_BOTH )
		{
			ShowError("buildin_setrank: Invalid type %d.\n", type);
			st->state = END;
			return SCRIPT_CMD_FAILURE;
		}
	}

	if( (rank = ranked_search(rank_id)) == NULL )
	{
		ShowError("buildin_setrank: Rank not found %d.\n", rank_id);
		st->state = END;
		return SCRIPT_CMD_FAILURE;
	}

	if( script_mapid2sd(4,sd) ) {
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}
	
	type++;
	script_pushint(st,ranked_set_rank(sd,rank_id,type));
	return SCRIPT_CMD_SUCCESS;
}

BUILDIN_FUNC(setrankperiod)
{
	struct map_session_data *sd = NULL;
	struct s_mapiterator *iter = NULL;
	int day, month, year, type = script_getnum(st,2);

	if( script_hasdata(st,3) ) {
		const char *buff = script_getstr(st,3);
		if( !buff || !*buff || strlen(buff) < 10 || sscanf(buff,"%04d-%02d-%02d", &year, &month, &day) < 3 ) {
			ShowError("buildin_setrankperiod: Invalid Date '%s', he value must be YYYY-MM-DD.\n", buff);
			script_pushint(st,0);
			return SCRIPT_CMD_FAILURE;
		}
		else if( month < 1 || month > 12 ) {
			ShowError("buildin_setrankperiod: Invalid Month '%d', min 1 and max 12.\n", month);
			script_pushint(st, 0);
			return SCRIPT_CMD_FAILURE;
		}
		else if( day < 1 || day > 31 ) {
			ShowError("buildin_setrankperiod: Invalid Day '%d', min 1 and max 31.\n", day);
			script_pushint(st, 0);
			return SCRIPT_CMD_FAILURE;
		}
	}
	else {
		day = date_get(DT_DAYOFMONTH);
		month = date_get(DT_MONTH);
		year = date_get(DT_YEAR);
	}

	if( SQL_ERROR == Sql_Query(qsmysql_handle,
		"REPLACE INTO `ranked_char2` (`char_id`, `date`, `rank_id`, `points_gained`, `points_lost`, `kills`, `deaths`, `damage_given`, `damage_taken`, `skill_success`, `skill_fail`, `skill_support_success`, `skill_support_fail`, `skill_damage_given`, `skill_damage_taken`, `heal_hp`, `heal_sp`) "
		"SELECT `char_id`, '%04d-%02d-%02d', `rank_id`, `points_gained`, `points_lost`, `kills`, `deaths`, `damage_given`, `damage_taken`, `skill_success`, `skill_fail`, `skill_support_success`, `skill_support_fail`, `skill_damage_given`, `skill_damage_taken`, `heal_hp`, `heal_sp` FROM `ranked_char`",
		year, month, day
	) ) {
		Sql_ShowDebug(qsmysql_handle);
		script_pushint(st, 0);
		return 0;
	}

	if( SQL_ERROR == Sql_Query(qsmysql_handle,
		"REPLACE INTO `ranked_account2` (`account_id`, `date`, `rank_id`, `points_gained`, `points_lost`, `kills`, `deaths`, `damage_given`, `damage_taken`, `skill_success`, `skill_fail`, `skill_support_success`, `skill_support_fail`, `skill_damage_given`, `skill_damage_taken`, `heal_hp`, `heal_sp`) "
		"SELECT `account_id`, '%04d-%02d-%02d', `rank_id`, `points_gained`, `points_lost`, `kills`, `deaths`, `damage_given`, `damage_taken`, `skill_success`, `skill_fail`, `skill_support_success`, `skill_support_fail`, `skill_damage_given`, `skill_damage_taken`, `heal_hp`, `heal_sp` FROM `ranked_account`",
		year, month, day
	)) {
		Sql_ShowDebug(qsmysql_handle);
		script_pushint(st,0);
		return 0;
	}

	if( type ) {
		iter = mapit_getallusers();
		for (sd = (TBL_PC*)mapit_first(iter); mapit_exists(iter); sd = (TBL_PC*)mapit_next(iter)) {
			// Reset Char
			sd->status.ranked_char.points.gained = 0;
			sd->status.ranked_char.points.lost = 0;
			memset(&sd->status.ranked_char.player, 0, sizeof(sd->status.ranked_char.player));
			memset(&sd->status.ranked_char.skill, 0, sizeof(sd->status.ranked_char.skill));
			memset(&sd->status.ranked_char.heal, 0, sizeof(sd->status.ranked_char.heal));

			// Reset Account
			sd->status.ranked_account.points.gained = 0;
			sd->status.ranked_account.points.lost = 0;
			memset(&sd->status.ranked_account.player, 0, sizeof(sd->status.ranked_account.player));
			memset(&sd->status.ranked_account.skill, 0, sizeof(sd->status.ranked_account.skill));
			memset(&sd->status.ranked_account.heal, 0, sizeof(sd->status.ranked_account.heal));

			// Save Char
			chrif_save(sd, CSAVE_NORMAL);
		}
		mapit_free(iter);
	

		// Reset in Sql
		if( SQL_ERROR == Sql_Query(qsmysql_handle, "UPDATE `ranked_char` SET `points_gained`='0', `points_lost`='0', `kills`='0', `deaths`='0', `damage_given`='0', `damage_taken`='0', `skill_success`='0', `skill_fail`='0', `skill_support_success`='0', `skill_support_fail`='0', `skill_damage_given`='0', `skill_damage_taken`='0', `heal_hp`='0', `heal_sp`='0'") ) {
			Sql_ShowDebug(qsmysql_handle);
			script_pushint(st, 0);
			return 0;
		}

		if( SQL_ERROR == Sql_Query(qsmysql_handle, "UPDATE `ranked_account` SET `points_gained`='0', `points_lost`='0', `kills`='0', `deaths`='0', `damage_given`='0', `damage_taken`='0', `skill_success`='0', `skill_fail`='0', `skill_support_success`='0', `skill_support_fail`='0', `skill_damage_given`='0', `skill_damage_taken`='0', `heal_hp`='0', `heal_sp`='0'") ) {
			Sql_ShowDebug(qsmysql_handle);
			script_pushint(st, 0);
			return 0;
		}
	}
	
	script_pushint(st,1);
	return SCRIPT_CMD_SUCCESS;
}
