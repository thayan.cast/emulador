#ifndef _CREATIVESDMMO_HPP_
#define _CREATIVESDMMO_HPP_

#include "../config.hpp"	 // For Configs

// Common Functions
#define member_size(type, member) sizeof(((type *)0)->member)
#define member_len(type, member) (sizeof(((type *)0)->member)/sizeof(((type *)0)->member[0]))

#define increment_limit(a, b, max) \
	do { \
		if( (max - a) < b ) { \
			a = max; \
		} else { \
			a += b; \
		} \
	} while(0)

#define decrement_limit(a, b, min) \
	do { \
		if( (b + min) > a ) { \
			a = min; \
		} else { \
			a -= b; \
		} \
	} while(0)

#endif /* _CREATIVESDMMO_HPP_ */
