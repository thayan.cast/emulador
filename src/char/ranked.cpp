// � Creative Services and Development
// Site Oficial: www.creativesd.com.br
// Termos de Contrato e Autoria em: http://creativesd.com.br/?p=termos

#ifndef _WIN32
#include <unistd.h>
#else
#include "../common/winapi.hpp"
#endif
#include "../common/cbasetypes.hpp"
#include "../common/sql.hpp"
#include "../common/strlib.hpp"
#include "../common/showmsg.hpp"
#include "../common/socket.hpp"
#include "../common/db.hpp"
#include "../common/mmo.hpp"

#include "ranked.hpp"
#include "inter.hpp"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

// Ranked System
bool ranked_load(uint32 id, enum ranked_storage tableswitch, struct ranked_status *rdata)
{
	char* data;
	const char *tablename, *selectoption;

	switch(tableswitch)
	{
		case RANKED_STORAGE_CHAR:
			tablename = "ranked_char";
			selectoption = "char_id";
			break;
		case RANKED_STORAGE_ACCOUNT:
			tablename = "ranked_account";
			selectoption = "account_id";
			break;
		default:
			ShowError("char_ranked_load: Invalid table name!\n");
			return false;
	}

	if( SQL_ERROR == Sql_Query(sql_handle, "SELECT "
		"`rank_id`, `points`, `points_gained`, `points_lost`,"
		"`kills`, `deaths`, `damage_given`, `damage_taken`,"
		"`skill_success`, `skill_fail`, `skill_support_success`, `skill_support_fail`,"
		"`skill_damage_given`, `skill_damage_taken`, `heal_hp`, `heal_sp`"
		" FROM `%s` WHERE `%s`='%d'", tablename, selectoption, id) )
	{
		Sql_ShowDebug(sql_handle);
		return false;
	}

	if( SQL_SUCCESS != Sql_NextRow(sql_handle) )
	{
		Sql_FreeResult(sql_handle);
		return false;
	}

	Sql_GetData(sql_handle,  0, &data, NULL); rdata->id = atoi(data);
	Sql_GetData(sql_handle,  1, &data, NULL); rdata->points.current = atol(data);
	Sql_GetData(sql_handle,  2, &data, NULL); rdata->points.gained = atol(data);
	Sql_GetData(sql_handle,  3, &data, NULL); rdata->points.lost = atol(data);
	Sql_GetData(sql_handle,  4, &data, NULL); rdata->player.kills = atol(data);
	Sql_GetData(sql_handle,  5, &data, NULL); rdata->player.deaths = atol(data);
	Sql_GetData(sql_handle,  6, &data, NULL); rdata->player.damage_given = atol(data);
	Sql_GetData(sql_handle,  7, &data, NULL); rdata->player.damage_taken = atol(data);
	Sql_GetData(sql_handle,  8, &data, NULL); rdata->skill.success = atol(data);
	Sql_GetData(sql_handle,  9, &data, NULL); rdata->skill.fail = atol(data);
	Sql_GetData(sql_handle, 10, &data, NULL); rdata->skill.support_success = atol(data);
	Sql_GetData(sql_handle, 11, &data, NULL); rdata->skill.support_fail = atol(data);
	Sql_GetData(sql_handle, 12, &data, NULL); rdata->skill.damage_given = atol(data);
	Sql_GetData(sql_handle, 13, &data, NULL); rdata->skill.damage_taken = atol(data);
	Sql_GetData(sql_handle, 14, &data, NULL); rdata->heal.hp = atol(data);
	Sql_GetData(sql_handle, 15, &data, NULL); rdata->heal.sp = atol(data);
	Sql_FreeResult(sql_handle);
	return true;
}

bool ranked_save(uint32 id, enum ranked_storage tableswitch, struct ranked_status *p, struct ranked_status *cp)
{
	const char *tablename, *selectoption;

	switch(tableswitch)
	{
		case RANKED_STORAGE_CHAR:
			tablename = "ranked_char";
			selectoption = "char_id";
			break;
		case RANKED_STORAGE_ACCOUNT:
			tablename = "ranked_account";
			selectoption = "account_id";
			break;
		default:
			ShowError("char_ranked_save: Invalid table name!\n");
			return false;
	}

	if(
		(p->id != cp->id) || (p->points.current != cp->points.current) || (p->points.gained != cp->points.gained) ||
		(p->points.lost != cp->points.lost) || (p->player.kills != cp->player.kills) || (p->player.deaths != cp->player.deaths) ||
		(p->player.damage_given != cp->player.damage_given) || (p->player.damage_taken != cp->player.damage_taken) ||
		(p->skill.success != cp->skill.success) || (p->skill.fail != cp->skill.fail) ||
		(p->skill.support_success != cp->skill.support_success) || (p->skill.support_fail != cp->skill.support_fail) ||
		(p->skill.damage_given != cp->skill.damage_given) || (p->skill.damage_taken != cp->skill.damage_taken) ||
		(p->heal.hp != cp->heal.hp) || (p->heal.sp != p->heal.sp)
	) {
		if( SQL_ERROR == Sql_Query(sql_handle, "REPLACE INTO `%s` (`%s`, `rank_id`, `points`, `points_gained`, `points_lost`, `kills`, `deaths`, `damage_given`, `damage_taken`, `skill_success`, `skill_fail`, `skill_support_success`, `skill_support_fail`, `skill_damage_given`, `skill_damage_taken`, `heal_hp`, `heal_sp`) VALUES (%d, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u, %u)", tablename, selectoption, id, p->id, p->points.current, p->points.gained, p->points.lost, p->player.kills, p->player.deaths, p->player.damage_given, p->player.damage_taken, p->skill.success, p->skill.fail, p->skill.support_success, p->skill.support_fail, p->skill.damage_given, p->skill.damage_taken, p->heal.hp, p->heal.sp) )
		{
			Sql_ShowDebug(sql_handle);
			return 0;
		}
	}
	return 1;
}
