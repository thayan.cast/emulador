#ifndef _CREATIVESDCHAR_HPP_
#define _CREATIVESDCHAR_HPP_

#include "../creativesd/config.hpp"	 // For Configs

#ifdef RESTOCK_SYSTEM_ENABLE
bool char_restock_load(uint32 char_id, struct item *item);
bool char_restock_save(uint32 char_id, struct mmo_charstatus* status);
#endif

#endif /* _CREATIVESDMAP_HPP_ */
