create table if not exists retirador_items
(
	nome_char varchar(255) not null,
	id_item int not null,
	quantidade_item int not null,
	motivo text not null,
	data timestamp default now() null
);

-- Bloquear delete na tabela de retirador_items
DROP TRIGGER IF EXISTS impededelete;
DELIMITER $$
CREATE TRIGGER impededelete BEFORE DELETE ON retirador_items
FOR EACH ROW BEGIN
  SIGNAL sqlstate '45001' set message_text = 'Bloqueado a dele��o.';
END;
$$

CREATE TABLE IF NOT EXISTS `ero_gm_reward` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`gm_aid` int(11) NOT NULL default '0',
	`gm_name` varchar(30) NOT NULL default '',
	`nameid` int(11) NOT NULL default '0',
	`amount` int(11) unsigned NOT NULL default '0',
	`refine` tinyint(3) unsigned NOT NULL default '0',
	`card1` smallint(4) unsigned NOT NULL default '0',
	`card2` smallint(4) unsigned NOT NULL default '0',
	`card3` smallint(4) unsigned NOT NULL default '0',
	`card4` smallint(4) unsigned NOT NULL default '0',
	`format_name` varchar(30) NOT NULL default 'null',
	`reward_time` datetime NOT NULL default '0000-00-00 00:00:00',
	`status` tinyint(1) NOT NULL default '0',
	`claim_aid` int(11) NOT NULL default '0',
	`claim_name` varchar(30) NOT NULL DEFAULT '',
	`claim_time` datetime NOT NULL default '0000-00-00 00:00:00',
	`note` varchar(255) NOT NULL default 'none',
	PRIMARY KEY  (`id`)
) ENGINE=MyISAM;

-- Bloquear delete na tabela de ero_gm_reward
DROP TRIGGER IF EXISTS impededelete;
DELIMITER $$
CREATE TRIGGER impededelete BEFORE DELETE ON ero_gm_reward
FOR EACH ROW BEGIN
  SIGNAL sqlstate '45002' set message_text = 'Bloqueado a dele��o.';
END;
$$