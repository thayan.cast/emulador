//*.-----------------------------------------------------------------.
// .    ____                          __                             .
// .   /\  _`\                       /\ \__  __                      .
// .   \ \ \/\_\  _ __    __     __  \ \ ,_\/\_\  __  __     __      .
// .    \ \ \/_/_/\`'__\/'__`\ /'__`\ \ \ \/\/\ \/\ \/\ \  /'__`\    .
// .     \ \ \s\ \ \ \//\  __//\ \d\.\_\ \ \_\ \ \ \ \_/ |/\  __/    .
// .      \ \____/\ \_\\ \____\ \__/.\_\\ \__\\ \_\ \___/ \ \____\   .
// .       \/___/  \/_/ \/____/\/__/\/_/ \/__/ \/_/\/__/   \/____/   .
// .                                                                 .
// .          2014~2019 � Creative Services and Development          .
// .                      www.creativesd.com.br                      .
// .-----------------------------------------------------------------.
// .                  CreativeSD Portugu�s (Brasil)                  .
// *-----------------------------------------------------------------*

// Ranked System
// ------------------------------------------------------------------
1610: Lista de Rank's
1611:  (ID: %d/%s) Pontos Necess�rio: %d
1612: Voc� caiu de Rank, voc� n�o est� em nenhum Rank!
1613: Parab�ns!!! Voc� alcan�ou o Rank %s.
1614: Voc� foi rebaixado para o Rank %s.
1615: Cuidado voc� est� em uma zona ranqueada!
1616: Banco de Dados de Rank re-carregado com sucesso!
1617: Voc� n�o est� ranqueado.
1618: Pontos: %d
1619: Rank Atual: %s
1620: Falta %d pontos para alcan�ar o Rank %s.
1621: Por favor, entre com o nome do jogador (usando: @rankinfo3 <jogador>).
1622: O Jogador '%s' n�o est� ranqueado.
1623: Jogador: %s
1624: Voc� s� pode desativar a exibi��o do seu Rank em Cidades.
1625: A exibi��o do seu Rank foi ativada.
1626: A exibi��o do seu Rank foi desativada.
1627: uso: @setrank <id_rank> <tipo>
1628:  Tipos:
1629:   0 - Somente Personagem
1630:   1 - Global da Conta
1631:   2 - Personagem e Conta
1632: Rank alterado com sucesso.
1633: Rank n�o encontrado.
1634: uso: @ranksetpoints <tipo> <quantidade>
1635: Voc� ganhou %d pontos por eliminar um jogador.
1636: Voc� perdeu %d pontos por ser eliminado.
1637: Voc� ganhou %d pontos por dar suporte.
1638: Voc� perdeu %d pontos por dar suporte incorreto.
1639: Voc� ganhou %d pontos.
1640: Voc� perdeu %d pontos.
1641: Voc� ganhou %d pontos por dano causado.
1642: Voc� perdeu %d pontos por receber muito dano.
1643: Voc� ganhou %d pontos por dano causado por Habilidades.
1644: Voc� perdeu %d pontos por receber muito dano de Habilidades.
1645: Voc� j� est� neste Rank.
1646: N�o foi poss�vel alterar seu Rank.