//*.-----------------------------------------------------------------.
// .    ____                          __                             .
// .   /\  _`\                       /\ \__  __                      .
// .   \ \ \/\_\  _ __    __     __  \ \ ,_\/\_\  __  __     __      .
// .    \ \ \/_/_/\`'__\/'__`\ /'__`\ \ \ \/\/\ \/\ \/\ \  /'__`\    .
// .     \ \ \s\ \ \ \//\  __//\ \d\.\_\ \ \_\ \ \ \ \_/ |/\  __/    .
// .      \ \____/\ \_\\ \____\ \__/.\_\\ \__\\ \_\ \___/ \ \____\   .
// .       \/___/  \/_/ \/____/\/__/\/_/ \/__/ \/_/\/__/   \/____/   .
// .                                                                 .
// .          2014~2018 © Creative Services and Development          .
// .                      www.creativesd.com.br                      .
// .-----------------------------------------------------------------.
// .                       CreativeSD English                        .
// *-----------------------------------------------------------------*

// Battleground Warfare
// ------------------------------------------------------------------
// System
1650: Server : %s has quit game...
1651: Server : %s is leaving the battlefield....
1652: Server : %s has been afk-kicked from the battlefield...
1653: Server : %s was kicked out for %s.
1654: Battleground: You're %s %s.
1655: You initiated the vote to expel %s player.
1656: You voted to expel %s player.
1657: Reason for Expulsion: %s
1658: You should report the reason for wanting to expel the %s player.
1659: %s initiated a vote to expel %s.
1660: Expel Player %s.
1661: Reason for Expulsion: %s
1662: %d/%d required for expulsion.
1663: Battleground: You received as a reward %dx %s in your inventory.
1664: Battleground: You received as a reward %dx %s in your Storage.
1665: Battleground: You received as a reward %dx %s.
1666: Battleground: You received as a reward (%d) %s.
1667: Battleground: You received as a reward +%d %s.
1668: There were no rewards for you in this Battle.
1669: You are in possession of the '%s'. 
1670: %s is in the possession of %s!
1671: You captured the '%s'. 
1672: You have recaptured the '%s'.
// Battleground Commands
1673: Unable to reload Battleground, the main script is not activated.
1674: A sign was sent to reload the Battleground.
1675: It was not possible to open the Battleground Registry.
1676: It was not possible to use the Quick Registry of Battleground.
1677: Could not remove it from the current battle.
1678: Removed from battle successfully.
1679: You are not participating in a Battleground.
1680: usage: @bgreportafk <player_name> to report a player who is absent from the battle.
1681: Player %s not found.
1682: Player %s is not in battle.
1683: You are not in the same Army as the player.
1684: Player is not absent, he is currently interacting with an NPC.
1685: Player is not absent.
1686: The Player has already been reported to the system.
1687: The Player was reported successfully.
1688: Battleground: You were reported for absence in battle, you have %d seconds to enter into interaction.
1689: It was not possible to open the Battleground Waiting List.
1690: It was not possible to open the Player List of Battleground.
1691: It was not possible to open the Battleground Rankings.
1692: You must enter the message (usage: @order <message>).
1693: This command is only for Army Captains.
1694: You have to be in a battle to send a message to your Army.
1695: You are not participating in a Battle.
1696: Only the Army Captain can use this command.
1697: There are no more players in your Army for you to name as Captain.
1698: This player is not on the same team as you.
1699: Player %s has been named Captain of the Army.
1700: Could not name Player %s as Army Captain.
1701: You must enter the player you want (usage: @bgchangeleader <Nickname>).
1702: Player: %s | Level: %d/%d | Job: %s
1703: Battleground Rewards re-charged successfully!
// Armys and patents
1704: Guillaume Army
1705: General Guillaume Marollo
1706: Blue Lions
1707: Only the strength of our Army can strengthen the Kingdom of Maroll.
1708: Croix Army
1709: Prince Croix Marollo
1710: Red Eagles
1711: Raise your swords one last time for the greater good of the Kingdom of Maroll.
1712: Captain
1713: Lieutenant
1714: Sergeant
1715: Soldier
1716: Battleground : %s joined the Army.

// Queue System
// ------------------------------------------------------------------
// System
1750: You can not enter the Queue in %s.
1751: To enter the Queue you must contain at least Base Level 30 %d.
1752: To enter the Queue you must contain at most Base Level %d.
1753: Player '%s' joined Queue at %s.
1754: You joined the Queue at %s.
1755: Player '%s' left the Queue at %s because he was not in a City.
1756: Player '%s' has exited Queue at %s.
1757: Player '%s' was kicked out of Queue at %s.
1758: Player '%s' was removed from Queue at %s by disconnecting.
1759: You were removed from Queue at %s because you left the City.
1760: You left the Queue at %s.
1761: You have been kicked from Queue at %s.
1762: You were removed from Queue at %s for not meeting the requirements.
1763: You have been removed from Queue at %s and must wait for %s to sign up again.
1764: You have been removed from Queue at %s because it has expired but you can now sign up for a new one.
1765: You were removed from Queue at %s because it has expired.
1766: %d seconds
1767: %d minutes and %d seconds
1768: %d hour %d minutes and %d seconds
1769: %d days %d hours %d minutes and %d seconds
// Commands
1770: Queues:
1771: No Queue found.
1772: Total %d Queues.
1773: You must inform the Queue where you want to know the registered players:
1774: There are no players waiting in the Queue at %s.
1775: Players waiting in Queue at %s:
1776:    (%d) %s | Job: %s | Level: %d/%d
1777: You must enter the character name or account id (usage: @queuekick <player_name/account_id>).
1778: The character was not found.
1779: The player is not listed on a Queue for you remove it.
1780: Player '%s' was successfully removed from Queue at %s:
1781: Failed to remove player '%s' from Queue at %s successfully:
1782: You must enter the delay value in seconds (usage: @queuesetdelay <delay>).
1783: Delay for Queue Entry changed successfully.
1784: Queue Delay: %s.
1785: You do not have time to wait for a Queue and can register normally.
1786: You will not be moved to another waiting queue if you are expiring.
1787: You will be moved to another waiting queue if you are expiring.

// Organized Shops
// ------------------------------------------------------------------
1790: You can not open more organized stores on this map.
1791: %s opened the Shop "%s" on: %s %d %d
1792: This is not a map of organized stores.
1793: There was a problem, all of the organized stores blocks are full.
1794: Recharged successfully, no more stores were moved from place to place.
1795: Recharged successfully, %d stores moved.

// Presence
// ------------------------------------------------------------------
1796: You have presence enabled, use the @bepresence command to turn off presence.

// Arealoot
// ------------------------------------------------------------------
1797: Arealoot is now off.
1798: Arealoot is now on.

// Event Room
// ------------------------------------------------------------------
1799: Could not teleport to the Events Room.

// WoE Manager
// ------------------------------------------------------------------
1800: Could not open WoE Manager.

// Extended Vending [CreativeSD/Lilith]
// ------------------------------------------------------------------
1801: You do not have enough CashPoint
1802: You do not have enough items
1803: Seller has not enough space in your inventory
1804: Seller can not take all the item
1805: You have selected: %s
1806: You've opened %s's shop. Sale is carried out: %s
1807: Current Currency: %s
1808: %s has bought '%s' in the amount  of %d. Revenue: %d %s
1809: Full revenue from %s is %d %s
1810: Cannot buy with Bound Items.
1811: Server
1812: Vending Report
1813: [%s] has bought in your store: \n
1814:    - %s x %d ea
1815: ......and more\r\n
1816: Total profit: %s x %d

// Cell PvP [brAthena]
// ------------------------------------------------------------------
1817: You can not create chat rooms within the pvp area.
1818: You can not enter chat rooms within the pvp area.
1819: You can not open stores within the pvp area.
1820: You can not view stores within the pvp area.
1820: Unable to exit this pvp area at this time. Wait %d seconds.
1821: You can not interact with NPCs within the pvp area.
1822: It is not possible to conduct negotiations here within the pvp area.
1823: Impossible to use teleport here within the pvp area.
1824: Unable to open the storage here inside the pvp area.
1825: Group invitations can not be sent within this pvp area.
1826: Guild invitations can not be sent within this pvp area.
1827: You can not drop items in this pvp area.
1828: His homunculus was vaporized because he can not stay within this pvp area.
1829: You can not call your homunculus here in this pvp area.
1830: Your pet has been returned to the egg because it can not stay inside this pvp area.
1831: Your pet can not stay inside this pvp area.
1832: You can not leave your group while inside this pvp area.
1833: You can not abandon your clan being inside this pvp area.
1834: ?????????????????????????????????????????????????????????????????????????????????????????
1835: Player '%s' (lv: %d) can not be attacked here because the level difference is very large.

// @refineui
// ------------------------------------------------------------------
1836: This command requires packet version 2016-10-12 or newer.
1837: This command is disabled via configuration.
1838: You have already opened the refine UI.

// Costume System
// ------------------------------------------------------------------
1839: Costume convertion is disable.
1840: You cannot costume this item.
1841: Costume

// Stuff Item
// ------------------------------------------------------------------
1842: Battleground's
1843: PvP's
1844: GvG's
1845: Please enter an item name or ID (usage: @stuffitem <item name/ID> <type> <quantity>).
1846: Types
1847:   1: Battleground
1848:	2: PvP
1849:   3: GvG
1850: Invalid type.
1851: Os Stuff para %s está desativado.

// No Storage MapFlags
// ------------------------------------------------------------------
1852: You can not open Storage on this map.
1853: You can not open Guild Storage on this map.

// Pet Evolution
// ------------------------------------------------------------------
1854: Could not evolve your Pet, try again.
1855: You do not have a Pet, make sure it's in the egg.
1856: It is not possible to evolve a Pet without its Egg.
1857: You do not have enough items to evolve your Pet.
1858: Check your inventory or digit @petevolutioninfo for more information.
1859: Your Pet's Loyalty is Low, Increase Loyalty to Evolve Your Pet.
1860: You can not further develop your Pet.
1861: Your Pet can progress to %s, list of requirements:
1862: Your Pet has evolved successfully!
1863: Auto Pet feeding is enabled.
1864: Auto Pet feeding is disabled.

// Pack Guild
// ------------------------------------------------------------------
1865: Pack Guild
1866: All Pack items have been removed because you have left the Guild.
1867: All Pack items have been removed because you have been expelled from the Clan.
1868: Enter the name or ID of an item (usage: @getguilditem <item name / ID> <quantity> <timeout>).
1869: Invalid type.
1870: Stuff for Pack Guild is disabled.